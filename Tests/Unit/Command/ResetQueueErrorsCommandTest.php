<?php
namespace ApacheSolrForTypo3\Solrfal\Tests\Unit\Context;

/***************************************************************
 * Copyright notice
 *
 * (c) 2017 Timo Hund <timo.hund@dkd.de>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use ApacheSolrForTypo3\Solrfal\Command\ResetQueueErrorsCommand;
use ApacheSolrForTypo3\Solrfal\Queue\ItemRepository;
use ApacheSolrForTypo3\Solrfal\Tests\Unit\UnitTest;
use Symfony\Component\Console\Formatter\OutputFormatterInterface;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class ResetQueueErrorsCommandTest
 */
class ResetQueueErrorsCommandTest extends UnitTest
{

    /**
     * @test
     */
    public function testCommandCallsFlushMethod()
    {
        $definitionMock = $this->getDumbMock(InputDefinition::class);
        /** @var $repositoryMock ItemRepository */
        $repositoryMock = $this->getDumbMock(ItemRepository::class);

        /** @var $command ResetQueueErrorsCommand */
        $command = $this->getMockBuilder(ResetQueueErrorsCommand::class)->disableOriginalConstructor()->setMethods(['getItemRepository'])->getMock();
        $command->setDefinition($definitionMock);
        $command->expects($this->once())->method('getItemRepository')->willReturn($repositoryMock);
        $repositoryMock->expects($this->once())->method('flushAllErrors')->willReturn(99);

        $inputMock = $this->getDumbMock(InputInterface::class);
        $outputMock = $this->getDumbMock(SymfonyStyle::class);
        $outputFormatterMock = $this->getDumbMock(OutputFormatterInterface::class);
        $outputMock->expects($this->any())->method('getFormatter')->willReturn($outputFormatterMock);

        $command->run($inputMock, $outputMock);
    }
}