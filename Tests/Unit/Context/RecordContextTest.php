<?php
namespace ApacheSolrForTypo3\Solrfal\Tests\Unit\Context;

/***************************************************************
 * Copyright notice
 *
 * (c) 2013 Steffen Ritter <steffen.ritter@typo3.org>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use ApacheSolrForTypo3\Solr\Access\Rootline;
use ApacheSolrForTypo3\Solr\Site;
use ApacheSolrForTypo3\Solrfal\Context\RecordContext;
use ApacheSolrForTypo3\Solrfal\Tests\Unit\UnitTest;

/**
 * Class InitializationAspectTest
 */
class RecordContextTest extends UnitTest
{

    /**
     * @var \ApacheSolrForTypo3\Solrfal\Context\RecordContext
     */
    protected $fixture;

    /**
     * @return void
     */
    public function setUp()
    {
        $site = $this->getDumbMock(Site::class);
        $site->expects($this->any())->method('getRootPageId')->will($this->returnValue('55'));

        $rootline = $this->getDumbMock(Rootline::class);
        $rootline->expects($this->any())->method('__toString')->will($this->returnValue('c:0'));

        $this->fixture = new RecordContext($site, $rootline, 'tt_news', 'media', 35, 0);
    }

    /**
     * @test
     * @return void
     */
    public function getContextIdentifierReturnsRecord()
    {
        $this->assertEquals('record', $this->fixture->getContextIdentifier());
    }

    /**
     * @test
     * @return void
     */
    public function constructorCallsParentConstructor()
    {
        $this->assertNotNull($this->fixture->getSite());
        $this->assertNotNull($this->fixture->getLanguage());
        $this->assertNotNull($this->fixture->getAccessRestrictions());
    }

    /**
     * @test
     * @return void
     */
    public function constructorInitializesVariables()
    {
        $this->assertEquals('tt_news', $this->fixture->getTable());
        $this->assertEquals('media', $this->fixture->getField());
        $this->assertEquals(35, $this->fixture->getUid());
    }

    /**
     * @test
     * @return void
     */
    public function toArrayContainsCorrectType()
    {
        $this->assertArrayHasKey('context_type', $this->fixture->toArray());
        $this->assertContains('record', $this->fixture->toArray());
    }

    /**
     * @test
     * @return void
     */
    public function toArrayContainsNewProperties()
    {
        $array = $this->fixture->toArray();
        $this->assertArrayHasKey('context_record_table', $array);
        $this->assertArrayHasKey('context_record_field', $array);
        $this->assertArrayHasKey('context_record_uid', $array);
    }
}
