<?php
namespace ApacheSolrForTypo3\Solrfal\Tests\Unit\Context;

/***************************************************************
 * Copyright notice
 *
 * (c) 2013 Steffen Ritter <steffen.ritter@typo3.org>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use ApacheSolrForTypo3\Solr\Access\Rootline;
use ApacheSolrForTypo3\Solr\Site;
use ApacheSolrForTypo3\Solr\System\Configuration\TypoScriptConfiguration;
use ApacheSolrForTypo3\Solrfal\Context\StorageContext;
use ApacheSolrForTypo3\Solrfal\Detection\StorageContextDetector;
use ApacheSolrForTypo3\Solrfal\Tests\Unit\UnitTest;
use TYPO3\CMS\Core\Log\Logger;
use TYPO3\CMS\Core\Resource\ResourceStorage;
use TYPO3\CMS\Core\Resource\StorageRepository;
use ApacheSolrForTypo3\Solrfal\Queue\ItemRepository;

/**
 * Class InitializationAspectTest
 */
class StorageContextTest extends UnitTest
{

    /**
     * @var \ApacheSolrForTypo3\Solrfal\Context\StorageContext
     */
    protected $fixture;

    /**
     * @return void
     */
    public function setUp()
    {
        $site = $this->getDumbMock(Site::class);
        $site->expects($this->any())->method('getRootPageId')->will($this->returnValue('55'));

        $rootline = $this->getDumbMock(Rootline::class);
        $rootline->expects($this->any())->method('__toString')->will($this->returnValue('c:0'));

        $this->fixture = new StorageContext($site, $rootline, 0, 'fileadmin');
    }

    /**
     * @test
     * @return void
     */
    public function getContextIdentifierReturnsStorage()
    {
        $this->assertEquals('storage', $this->fixture->getContextIdentifier());
    }

    /**
     * @test
     */
    public function toArrayContainsCorrectType()
    {
        $this->assertArrayHasKey('context_type', $this->fixture->toArray());
        $this->assertContains('storage', $this->fixture->toArray());
    }

    /**
     * @test
     */
    public function toArrayContainsIndexConfigurationName()
    {
        $storageData = $this->fixture->toArray();
        $this->assertArrayHasKey('context_record_indexing_configuration', $storageData);
        $this->assertSame('fileadmin', $storageData['context_record_indexing_configuration']);
    }

    /**
     * @test
     */
    public function initializeQueueTriggersInitializeQueueForStorage()
    {
        $fakeConfiguration = [
            'fileadmin.' => [
                'table' => 'sys_file_storage',
                'storageUid' => 4711
            ]
        ];

        $configurationMock = $this->getDumbMock(TypoScriptConfiguration::class);
        $configurationMock->expects($this->once())->method('getObjectByPathOrDefault')->will(
            $this->returnValue($fakeConfiguration)
        );

            /** @var $siteMock Site */
        $siteMock = $this->getDumbMock(Site::class);
        $siteMock->expects($this->once())->method('getSolrConfiguration')->will($this->returnValue(
            $configurationMock
        ));


        $storageMock = $this->getDumbMock(ResourceStorage::class);
        $storageRepositoryMock = $this->getDumbMock(StorageRepository::class);
        $storageRepositoryMock->expects($this->once())->method('findByUid')->will($this->returnValue($storageMock));

        $itemRepositoryMock = $this->getDumbMock(ItemRepository::class);

            // we fake the storageContext, that the indexing is enabled and a mocked logger will be used
            // in addition we mock initializeQueueForStorage to only check if it this method was triggered
            /** @var $storageContext StorageContextDetector */
        $storageContext = $this->getMockBuilder(StorageContextDetector::class)->setMethods(
            ['getLogger', 'initializeQueueForStorage', 'isIndexingEnabledForContext', 'isIndexingEnabledForStorage', 'getStorageRepository', 'getItemRepository']
        )->setConstructorArgs([$siteMock])->getMock();
        $storageContext->expects($this->once())->method('isIndexingEnabledForStorage')->will($this->returnValue(true));
        $storageContext->expects($this->once())->method('isIndexingEnabledForContext')->with('storage')->will($this->returnValue(true));
        $storageContext->expects($this->any())->method('getLogger')->will(
            $this->returnValue($this->getDumbMock(Logger::class))
        );
        $storageContext->expects($this->any())->method('getStorageRepository')->will(
            $this->returnValue($storageRepositoryMock)
        );
        $storageContext->expects($this->any())->method('getItemRepository')->will(
            $this->returnValue($itemRepositoryMock)
        );


        $storageContext->expects($this->once())->method('initializeQueueForStorage')->with($storageMock, 'fileadmin');

        $fakeStatus = ['fileadmin' => 1];
        $storageContext->initializeQueue($fakeStatus);
    }
}
