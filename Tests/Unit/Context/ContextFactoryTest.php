<?php
namespace ApacheSolrForTypo3\Solrfal\Tests\Unit\Context;

/***************************************************************
 * Copyright notice
 *
 * (c) 2013 Steffen Ritter <steffen.ritter@typo3.org>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use ApacheSolrForTypo3\Solr\Access\Rootline;
use ApacheSolrForTypo3\Solr\Domain\Site\SiteRepository;
use ApacheSolrForTypo3\Solr\Site;
use ApacheSolrForTypo3\Solrfal\Context\ContextFactory;
use ApacheSolrForTypo3\Solrfal\Context\ContextFactoryInterface;
use ApacheSolrForTypo3\Solrfal\Context\ContextInterface;
use ApacheSolrForTypo3\Solrfal\Context\PageContext;
use ApacheSolrForTypo3\Solrfal\Context\RecordContext;
use ApacheSolrForTypo3\Solrfal\Context\StorageContext;
use ApacheSolrForTypo3\Solrfal\Detection\RecordDetectionInterface;
use ApacheSolrForTypo3\Solrfal\Tests\Unit\UnitTest;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class InitializationAspectTest
 */
class ContextFactoryTest extends UnitTest
{

    /**
     * @var \ApacheSolrForTypo3\Solrfal\Context\ContextFactory
     */
    protected $fixture;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    protected $siteRepository;

    public function setUp()
    {
        $this->siteRepository = $this->getDumbMock(SiteRepository::class);
        $this->fixture = new ContextFactory($this->siteRepository);
    }

    /**
     * @test
     * @expectedException \RuntimeException
     * @return void
     */
    public function registerTypeThrowsExceptionIfClassIsNotImplementingContextInterface()
    {
        $this->fixture->registerType('newType', __CLASS__, __CLASS__);
    }

    /**
     * @test
     * @return void
     */
    public function registerTypeWorksIfInterfaceIsImplemented()
    {
        $this->fixture->registerType('newType',
            get_class($this->getDumbMock(ContextInterface::class)),
            get_class($this->getDumbMock(RecordDetectionInterface::class))
            );
    }

    /**
     * @test
     * @expectedException \RuntimeException
     * @return void
     */
    public function registerTypeThrowsExceptionForCustomFactoryNotImplementingTheInterface()
    {
        $this->fixture->registerType(
            'newType',
            get_class($this->getDumbMock(ContextInterface::class)),
            __CLASS__
        );
    }

    /**
     * @test
     * @return void
     */
    public function registerTypeAcceptsCorrectFactory()
    {
        $this->fixture->registerType(
            'newType',
            get_class($this->getDumbMock(ContextInterface::class)),
            get_class($this->getDumbMock(RecordDetectionInterface::class)),
            get_class($this->getDumbMock(ContextFactoryInterface::class))
        );
    }


    /**
     * @test
     * @expectedException \RuntimeException
     * @return void
     */
    public function getByRecordThrowsExceptionForUnsupportedType()
    {
        $this->fixture->getByRecord(['context_type' => 'foo']);
    }

    /**
     * @test
     * @return void
     */
    public function getByRecordUsesCustomFactoryIfRegistered()
    {
        $record = ['context_type' => 'foo'];

        $class = get_class($this->getDumbMock(ContextInterface::class));
        $factory = $this->getDumbMock(ContextFactoryInterface::class);
        $factory->expects($this->once())->method('getByRecord')->with($record);
        GeneralUtility::addInstance(get_class($factory), $factory);

        $detectorMock = $this->getDumbMock(RecordDetectionInterface::class);

        $this->fixture->registerType('foo', $class, get_class($detectorMock), get_class($factory));

        $this->fixture->getByRecord($record);

        GeneralUtility::purgeInstances();
    }


    /**
     * @test
     * @dataProvider getContextRecords
     * @param array $record
     * @param string $expectedClass
     */
    public function getByRecordCreatesBuildInContexts(array $record, $expectedClass)
    {
        $site = $this->getDumbMock(Site::class);
        $site->expects($this->any())->method('getRootPageId')->will($this->returnValue('55'));
        GeneralUtility::addInstance(Site::class, $site);

        $rootline = $this->getDumbMock(Rootline::class);
        $rootline->expects($this->any())->method('__toString')->will($this->returnValue('c:0'));

        GeneralUtility::addInstance(Rootline::class, $rootline);

        $this->siteRepository->expects($this->any())->method('getSiteByRootPageId')->will($this->returnValue($site));
        $this->assertInstanceOf($expectedClass, $this->fixture->getByRecord($record));

        GeneralUtility::purgeInstances();
    }

    /**
     * @return array
     */
    public static function getContextRecords()
    {
        return [
            'Storage' => [['context_type' => 'storage'], StorageContext::class],
            'Page' => [['context_type' => 'page'], PageContext::class],
            'Record' => [['context_type' => 'record'], RecordContext::class]
        ];
    }
}
