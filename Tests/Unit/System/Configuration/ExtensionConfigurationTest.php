<?php
namespace ApacheSolrForTypo3\Solrfal\Tests\Unit\System\Configuration;

/***************************************************************
 * Copyright notice
 *
 * (c) 2017 Timo Hund <timo.hund@dkd.de>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use ApacheSolrForTypo3\Solrfal\System\Configuration\ExtensionConfiguration;
use ApacheSolrForTypo3\Solrfal\Tests\Unit\UnitTest;

/**
 * Class ExtensionConfigurationTest
 *
 */
class ExtensionConfigurationTest extends UnitTest
{

    /**
     * @test
     */
    public function testGetIsSiteExclusiveRecordTable()
    {
        $configuration = new ExtensionConfiguration([
            'siteExclusiveRecordTables' => 'pages, pages_language_overlay, tt_content, sys_file_reference'
        ]);

        $this->assertFalse($configuration->getIsSiteExclusiveRecordTable('tx_news_domain_model_news'));
        $this->assertTrue($configuration->getIsSiteExclusiveRecordTable('pages'));
    }
}