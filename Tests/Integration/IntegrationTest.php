<?php
namespace ApacheSolrForTypo3\Solrfal\Tests\Integration;

/***************************************************************
 * Copyright notice
 *
 * (c) 2016 Markus Friedrich <markus.friedrich@dkd.de>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use ApacheSolrForTypo3\Solr\Tests\Integration\IntegrationTest as SolrIntegrationTest;
use ApacheSolrForTypo3\Solr\Site;
use ApacheSolrForTypo3\Solr\Domain\Site\SiteRepository;
use ApacheSolrForTypo3\Solrfal\Queue\ItemRepository;
use TYPO3\CMS\Core\Charset\CharsetConverter;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Lang\LanguageService;

/**
 * Base class for all integration tests in the EXT:solrfal project
 *
 * @author Markus Friedrich
 * @package TYPO3
 * @subpackage solrfal
 */
abstract class IntegrationTest extends SolrIntegrationTest
{

    /**
     * @var array
     */
    protected $testExtensionsToLoad = ['typo3/sysext/extbase', 'typo3conf/ext/solr', 'typo3conf/ext/solrfal'];

    /**
     * Temporary files
     *
     * @var array $temporaryFiles
     */
    protected $temporaryFiles = [];

    /**
     * @var \ApacheSolrForTypo3\Solrfal\Queue\ItemRepository
     */
    protected $itemRepository;

    /**
     * Returns the data handler
     *
     * @return \TYPO3\CMS\Core\DataHandling\DataHandler
     */
    protected function getDataHandler()
    {

        $GLOBALS['LANG'] = GeneralUtility::makeInstance(LanguageService::class);

        $csConf = GeneralUtility::makeInstance(CharsetConverter::class);
        $GLOBALS['LANG']->csConvObj = $csConf;

        return GeneralUtility::makeInstance(DataHandler::class);
    }

    /**
     * Place temporary file
     *
     * @param string $fixtureFileName
     * @param string $targetDirectory, e.g. 'fileadmin'
     * @return void
     */
    protected function placeTemporaryFile($fixtureFileName, $targetDirectory)
    {

        // create directory
        $dirPath = PATH_site . rtrim($targetDirectory, '/');
        if (!is_dir($dirPath)) {
            mkdir($dirPath, 0777, true);
        }

        // place file
        $filePath =  $dirPath . '/' . $fixtureFileName;
        if (copy($this->getFixturePathByName($fixtureFileName), $filePath)) {
            $this->temporaryFiles[] = $filePath;
        }

        $this->assertEquals(true, is_file($this->getFixturePathByName($fixtureFileName)), 'Couldn\'t find source of temporary file:' . $this->getFixturePathByName($fixtureFileName));
        $this->assertEquals(true, is_file($filePath), 'Couldn\'t place temporary file: ' . $filePath);
    }

    /**
     * Removes temporary files
     *
     * @return void
     */
    protected function removeTemporaryFiles()
    {
        foreach ($this->temporaryFiles as $temporaryFile) {
            unlink($temporaryFile);
        }
        $this->temporaryFiles = [];
    }

    /**
     * Returns the storage context detector
     *
     * @param integer $rootPageId
     * @return Site
     */
    protected function getSite($rootPageId)
    {
        $siteRepository = GeneralUtility::makeInstance(SiteRepository::class);
        return $siteRepository->getSiteByRootPageId($rootPageId);
    }

    /**
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        // init deprecation log
        $integrationTestDeprecationLog = GeneralUtility::makeInstance(IntegrationTestDeprecationLog::class);
        $integrationTestDeprecationLog->initialize();

        $this->setUpBackendUserFromFixture(1);
        $this->itemRepository = GeneralUtility::makeInstance(ItemRepository::class);
    }

    /**
     * @return void
     */
    public function tearDown()
    {
        $this->assertEquals(0, count($GLOBALS['T3_VAR']['ext']['solrfal']['deprecationLog']), 'Deprecated functions used: ' . implode("\n", $GLOBALS['T3_VAR']['ext']['solrfal']['deprecationLog']));
        $this->removeTemporaryFiles();
        parent::tearDown();
    }

    /**
     * @return SiteRepository
     */
    protected function getSiteRepository()
    {
        return GeneralUtility::makeInstance(SiteRepository::class);
    }
}
