<?php
namespace ApacheSolrForTypo3\Solrfal\Tests\Integration\Indexing;

/***************************************************************
 * Copyright notice
 *
 * (c) 2016 Markus Friedrich <markus.friedrich@dkd.de>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use ApacheSolrForTypo3\Solr\Util;
use ApacheSolrForTypo3\Solrfal\Indexing\DocumentFactory;
use ApacheSolrForTypo3\Solrfal\Queue\ItemRepository;
use ApacheSolrForTypo3\Solrfal\Tests\Integration\IntegrationTest;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Document factory tests
 *
 * @author Markus Friedrich
 * @package TYPO3
 * @subpackage solrfal
 */
class DocumentFactoryTest extends IntegrationTest
{

    /**
     * Item repository
     *
     * @var \ApacheSolrForTypo3\Solrfal\Queue\ItemRepository
     */
    protected $itemRepository;

    /**
     * The document factory
     *
     * @var \ApacheSolrForTypo3\Solrfal\Indexing\DocumentFactory
     */
    protected $documentFactory;

    /**
     * Set up document factory tests
     *
     * @return void
     */
    public function setUp()
    {
        $this->testExtensionsToLoad[] = 'typo3/sysext/filemetadata';

        parent::setUp();

        $this->importDumpFromFixture('fake_extension_table.sql');
        $GLOBALS['TCA']['tx_fakeextension_domain_model_news'] = include($this->getFixturePathByName('fake_extension_tca.php'));

        $this->documentFactory = GeneralUtility::makeInstance(DocumentFactory::class);
        $this->itemRepository = GeneralUtility::makeInstance(ItemRepository::class);
    }

    /**
     * Tests the document creation in record context
     *
     * @test
     * @return void
     */
    public function createDocumentInRecordContext()
    {
        $this->importDataSetFromFixture('index_file_in_record_context.xml');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        $queueItem = $this->itemRepository->findByUid(1);
        Util::initializeTsfe(1, 0);
        $document = $this->documentFactory->createDocumentForQueueItem($queueItem);

        $this->assertEquals('Lorem ipsum', $document->getField('fileReferenceTitle')['value'], 'Reference title not set correctly!');
        $this->assertEquals('title sys_file_metadata', $document->getField('metaDataTitle')['value'], 'File meta data title not set correctly');
        $this->assertEquals(true, in_array('fileReferenceUrl', $document->getFieldNames()), 'Reference URL not set!');
        $this->assertNotEmpty($document->getField('fileReferenceUrl')['value'], 'Invalid reference URL!');
        $this->assertEquals(1, $document->getField('fieldDefinedViaGlobalConfiguration_intS')['value'], 'Field "fieldDefinedViaGlobalConfiguration_intS", defined via global configuration, isn\'t set correctly');
        $this->assertEquals(1, $document->getField('fieldDefinedViaGlobalRecordContextConfiguration_intS')['value'], 'Field "fieldDefinedViaGlobalRecordContextConfiguration_intS", defined via global record context configuration, isn\'t set correctly');
        $this->assertEquals(1, $document->getField('fieldDefinedViaRecordContextTableConfiguration_intS')['value'], 'Field "fieldDefinedViaRecordContextTableConfiguration_intS", defined via table specific record context configuration, isn\'t set correctly');
        $this->assertEquals('081dbd21084d40f28002304ab2b6b739200c52b9/tx_solr_file/9999', $document->getField('variantId')['value'], 'Field "variantId" did not contain expected content');
    }

    /**
     * Tests the document creation in storage context
     *
     * @test
     * @return void
     */
    public function createDocumentInStorageContext()
    {
        $this->importDataSetFromFixture('index_file_in_storage_context.xml');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        $queueItem = $this->itemRepository->findByUid(1);
        Util::initializeTsfe(1, 0);
        $document = $this->documentFactory->createDocumentForQueueItem($queueItem);

        $this->assertEquals('r:1,2', $document->getField('access')['value'], 'File permissions not set correctly');
        $this->assertEquals('title sys_file_metadata', $document->getField('metaDataTitle')['value'], 'File meta data title not set correctly');
        $this->assertEquals(1, $document->getField('fieldDefinedViaGlobalConfiguration_intS')['value'], 'Field "fieldDefinedViaGlobalConfiguration_intS", defined via global configuration, isn\'t set correctly');
        $this->assertEquals(1, $document->getField('fieldDefinedViaGlobalStorageContextConfiguration_intS')['value'], 'Field "fieldDefinedViaGlobalStorageContextConfiguration_intS", defined via global storage context configuration, isn\'t set correctly');
        $this->assertEquals(1, $document->getField('fieldDefinedViaSpecificStorageContextConfiguration_intS')['value'], 'Field "fieldDefinedViaSpecificStorageContextConfiguration_intS", defined via table specific storage context configuration, isn\'t set correctly');
        $this->assertEquals('081dbd21084d40f28002304ab2b6b739200c52b9/tx_solr_file/9999', $document->getField('variantId')['value'], 'Field "variantId" did not contain expected content');
    }

    /**
     * Tests the document creation in page context
     *
     * @test
     * @return void
     */
    public function createDocumentInPageContext()
    {
        $this->importDataSetFromFixture('index_file_in_page_context.xml');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        $queueItem = $this->itemRepository->findByUid(1);
        Util::initializeTsfe(1, 0);
        $document = $this->documentFactory->createDocumentForQueueItem($queueItem);

        $this->assertEquals(1, $document->getField('fileReferenceUrl')['value'], 'Field "fileReferenceUrl" isn\'t filled with page id, as expected');
        $this->assertEquals(1652220000, $document->getField('endtime')['value'], 'Access field "endtime" isn\'t set correctly!');
        $this->assertEquals('Hello Solr', $document->getField('fileReferenceTitle')['value'], 'Field "fileReferenceTitle" isn\'t filled with page title, as expected');
        $this->assertEquals(1, $document->getField('fieldDefinedViaGlobalConfiguration_intS')['value'], 'Field "fieldDefinedViaGlobalConfiguration_intS", defined via global configuration, isn\'t set correctly');
        $this->assertEquals(1, $document->getField('fieldDefinedViaPageContextConfiguration_intS')['value'], 'Field "fieldDefinedViaPageContextConfiguration_intS", defined via page context configuration, isn\'t set correctly');
        $this->assertEquals('081dbd21084d40f28002304ab2b6b739200c52b9/tx_solr_file/9999', $document->getField('variantId')['value'], 'Field "variantId" did not contain expected content');
    }
}
