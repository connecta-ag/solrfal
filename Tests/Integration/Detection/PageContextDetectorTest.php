<?php
namespace ApacheSolrForTypo3\Solrfal\Tests\Integration\Detection;

/***************************************************************
 * Copyright notice
 *
 * (c) 2016 Markus Friedrich <markus.friedrich@dkd.de>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use ApacheSolrForTypo3\Solr\Access\Rootline;
use ApacheSolrForTypo3\Solr\Domain\Index\IndexService;
use ApacheSolrForTypo3\Solr\Util;
use ApacheSolrForTypo3\Solrfal\Detection\PageContextDetectorFrontendIndexingAspect;
use ApacheSolrForTypo3\Solrfal\Queue\Queue;
use ApacheSolrForTypo3\Solrfal\Tests\Integration\IntegrationTest;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Registry;

/**
 * Page context detector tests
 *
 * @author Markus Friedrich
 * @package TYPO3
 * @subpackage solrfal
 */
class PageContextDetectorTest extends IntegrationTest
{

    /**
     * @test
     */
    public function detectContentDeletion()
    {
        $this->importDataSetFromFixture('PageContext/observes_deletions.xml');
        $this->placeTemporaryFile('file8888.pdf', 'fileadmin');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        $dataHandler = $this->getDataHandler();
        $dataHandler->start([], ['tt_content' => [10 => ['delete' => 1]]]);
        $dataHandler->process_cmdmap();

        $indexQueueContent = $this->itemRepository->findAll();
        $this->assertEquals(1, count($indexQueueContent), 'Index queue not updated as expected, deletion of sys_file_reference ignored.');
        $this->assertEquals(8888, $indexQueueContent[0]->getFile()->getUid(), 'Remaining file in index queue is not "file8888.pdf" as expected.');
    }

    /**
     * @test
     */
    public function detectRelationDeletion()
    {
        $this->importDataSetFromFixture('PageContext/observes_deletions.xml');
        $this->placeTemporaryFile('file8888.pdf', 'fileadmin');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        $dataHandler = $this->getDataHandler();
        $dataHandler->start([], ['sys_file_reference' => [9999 => ['delete' => 1]]]);
        $dataHandler->process_cmdmap();

        $indexQueueContent = $this->itemRepository->findAll();
        $this->assertEquals(1, count($indexQueueContent), 'Relation deletion ignored, file "file9999.txt" is still in queue.');
        $this->assertEquals(8888,  $indexQueueContent[0]->getFile()->getUid(), 'Remaining file in index queue is not "file8888.pdf" as expected.');
    }

    /**
     * @test
     */
    public function detectPageDeletion()
    {
        $this->importDataSetFromFixture('PageContext/observes_deletions.xml');
        $this->placeTemporaryFile('file8888.pdf', 'fileadmin');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        $dataHandler = $this->getDataHandler();
        $dataHandler->start([], ['pages' => [2 => ['delete' => 1]]]);
        $dataHandler->process_cmdmap();

        $indexQueueContent = $this->itemRepository->findAll();
        $this->assertEquals(0, count($indexQueueContent), 'Page deletion didn\'t trigger the file deletion.');
    }

    /**
     * @test
     */
    public function detectContentHiding()
    {
        $this->importDataSetFromFixture('PageContext/observes_deletions.xml');
        $this->placeTemporaryFile('file8888.pdf', 'fileadmin');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        $dataHandler = $this->getDataHandler();
        $dataHandler->start(['tt_content' => [10 => ['hidden' => 1]]], []);
        $dataHandler->process_datamap();

        $indexQueueContent = $this->itemRepository->findAll();
        $this->assertEquals(1, count($indexQueueContent), 'Index queue not updated as expected, hiding of tt_content record ignored.');
        $this->assertEquals(8888, $indexQueueContent[0]->getFile()->getUid(), 'Remaining file in index queue is not "file8888.pdf" as expected.');
    }

    /**
     * @test
     */
    public function detectRelationHiding()
    {
        $this->importDataSetFromFixture('PageContext/observes_deletions.xml');
        $this->placeTemporaryFile('file8888.pdf', 'fileadmin');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        $dataHandler = $this->getDataHandler();
        $dataHandler->start(['sys_file_reference' => [9999 => ['hidden' => 1]]], []);
        $dataHandler->process_datamap();

        $indexQueueContent = $this->itemRepository->findAll();
        $this->assertEquals(1, count($indexQueueContent), 'Relation hiding ignored, file "file9999.txt" is still in queue.');
        $this->assertEquals(8888,  $indexQueueContent[0]->getFile()->getUid(), 'Remaining file in index queue is not "file8888.pdf" as expected.');
    }

    /**
     * @test
     */
    public function detectPageHiding()
    {
        $this->importDataSetFromFixture('PageContext/observes_deletions.xml');
        $this->placeTemporaryFile('file8888.pdf', 'fileadmin');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        $dataHandler = $this->getDataHandler();
        $dataHandler->start(['pages' => [2 => ['hidden' => 1]]], []);
        $dataHandler->process_datamap();

        $indexQueueContent = $this->itemRepository->findAll();
        $this->assertEquals(0, count($indexQueueContent), 'Page hidding didn\'t trigger the file deletion.');
    }

    /**
     * This testcase checks if we can create a new testpage on the root level without any errors.
     *
     * @test
     */
    public function canCreateSiteOneRootLevel()
    {
        $this->importDataSetFromFixture('PageContext/observes_create.xml');
        $this->placeTemporaryFile('file8888.pdf', 'fileadmin');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        $this->assertSolrQueueContainsAmountOfItems(0);
        $dataHandler = $this->getDataHandler();
        $dataHandler->start(['pages' => ['NEW' => ['hidden' => 0]]], []);
        $dataHandler->process_datamap();

        // the item is outside a siteroot so we should not have any queue entry
        $this->assertSolrQueueContainsAmountOfItems(0);
    }

    /**
     * This testcase checks if we can create a new testpage on the root level without any errors.
     *
     * @test
     */
    public function canCreateSubPageBelowSiteRoot()
    {
        $this->importDataSetFromFixture('PageContext/observes_create.xml');
        $this->placeTemporaryFile('file8888.pdf', 'fileadmin');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');

        $this->assertSolrQueueContainsAmountOfItems(0);
        $dataHandler = $this->getDataHandler();
        $dataHandler->start(['pages' => ['NEW' => ['hidden' => 0, 'pid' => 1]]], []);
        $dataHandler->process_datamap();

        // we should have one item in the solr queue
        $this->assertSolrQueueContainsAmountOfItems(1);
    }

    /**
     * Checks if files attached to access protected content elements can be handled
     *
     * @test
     */
    public function canHandleAccessProtectedContentElements()
    {
        $this->importDataSetFromFixture('PageContext/handles_access_protected_content_elements.xml');
        $this->placeTemporaryFile('file8888.pdf', 'fileadmin');
        $this->placeTemporaryFile('file9999.txt', 'fileadmin');
        Util::initializeTsfe(2, 0, false);

        $methodAddDetectedFiles = new \ReflectionMethod(PageContextDetectorFrontendIndexingAspect::class, 'addDetectedFilesToPage');
        $methodAddDetectedFiles->setAccessible(true);

        // simulate indexing of public contens
        $GLOBALS['TSFE']->gr_list = '0,-2,0';
        $pageContextDetectorFrontendIndexingAspect = $this->getPageContextDetectorFrontendIndexingAspect([8888], [10 => BackendUtility::getRecord('tt_content', 10)]);
        $this->assertSame(0, $this->itemRepository->count(), 'File Index Queue is not empty as expected.');
        $pageAccessRootline = GeneralUtility::makeInstance(Rootline::class, 'c:0');
        $methodAddDetectedFiles->invokeArgs($pageContextDetectorFrontendIndexingAspect, [$GLOBALS['TSFE'], $pageAccessRootline]);
        $this->assertSame(1, $this->itemRepository->count(), 'PageContextDetector didn\'t detect exactly 1 document as expected.');

        // simulate indexing of access protected contents
        $GLOBALS['TSFE']->gr_list = '0,-2,1';
        $pageContextDetectorFrontendIndexingAspect = $this->getPageContextDetectorFrontendIndexingAspect([9999], [20 => BackendUtility::getRecord('tt_content', 20)]);
        $this->assertSame(1, $this->itemRepository->count(), 'File Index Queue didn\'t contain the document detected in last run.');
        $pageAccessRootline = GeneralUtility::makeInstance(Rootline::class, 'c:1');
        $methodAddDetectedFiles->invokeArgs($pageContextDetectorFrontendIndexingAspect, [$GLOBALS['TSFE'], $pageAccessRootline]);
        $this->assertSame(2, $this->itemRepository->count(), 'File Index Queue didn\'t contain the 2 documents from the two detection runs.');
    }

    /**
     * Checks if detected files were resetted after item indexing
     *
     * @test
     */
    public function canResetDetectedFileUids()
    {
        $this->importDataSetFromFixture('PageContext/handles_access_protected_content_elements.xml');
        $registry = GeneralUtility::makeInstance(Registry::class);
        $registry->set('tx_solrfal', 'pageContextDetector.successfulFileUids', [1, 2, 3]);

        $site = $this->getSiteRepository()->getSiteByRootPageId(1);
        $indexService = GeneralUtility::makeInstance(IndexService::class, $site);
        $methodEmitSignal = new \ReflectionMethod(IndexService::class, 'emitSignal');
        $methodEmitSignal->setAccessible(true);
        $methodEmitSignal->invokeArgs($indexService, ['afterIndexItem', []]);

        $successfulFileUids = $registry->get('tx_solrfal', 'pageContextDetector.successfulFileUids');
        $this->assertNull($successfulFileUids, 'Detected file uids were not resetted by using signal "afterIndexItem".');
    }

    /**
     * @param integer $assertedItemCount
     */
    protected function assertSolrQueueContainsAmountOfItems($assertedItemCount)
    {
        /** @var $indexQueue Queue */
        $indexQueue = GeneralUtility::makeInstance(Queue::class);
        $this->assertSame($assertedItemCount, $indexQueue->getAllItemsCount(), 'EXT:solr index queue does not contain expected item amount');
    }

    /**
     * Returns the PageContextDetectorFrontendIndexingAspect
     *
     * @param array $collectedFileUids
     * @param array $collectedContentElements
     * @return PageContextDetectorFrontendIndexingAspect
     */
    protected function getPageContextDetectorFrontendIndexingAspect(array $collectedFileUids, array $collectedContentElements)
    {
        $pageContextDetectorFrontendIndexingAspect = GeneralUtility::makeInstance(PageContextDetectorFrontendIndexingAspect::class);
        $pageContextDetectorFrontendIndexingAspect::$collectedFileUids = $collectedFileUids;
        $pageContextDetectorFrontendIndexingAspect::$collectedContentElements = $collectedContentElements;
        return $pageContextDetectorFrontendIndexingAspect;
    }
}
