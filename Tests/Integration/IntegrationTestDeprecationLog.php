<?php
namespace ApacheSolrForTypo3\Solrfal\Tests\Integration;

/***************************************************************
 * Copyright notice
 *
 * (c) 2016 Markus Friedrich <markus.friedrich@dkd.de>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Tests\FunctionalTestCase;

/**
 * Logs deprecation messages
 *
 * @author Markus Friedrich
 * @package TYPO3
 * @subpackage solrfal
 */
class IntegrationTestDeprecationLog extends FunctionalTestCase implements SingletonInterface
{

    /**
     * Initializes the deprecation log for solrfal integration tests
     *
     * @return void
     */
    public function initialize()
    {
        $GLOBALS['TYPO3_CONF_VARS']['SYS']['enableDeprecationLog'] = 'devlog';
        $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_div.php']['devLog'] = [
            'solrfal' => 'ApacheSolrForTypo3\Solrfal\Tests\Integration\IntegrationTestDeprecationLog->devLog'
        ];
        $GLOBALS['T3_VAR']['ext']['solrfal']['deprecationLog'] = [];
    }

    /**
     * Developer log
     *
     * $logArr = ['msg'=>$msg, 'extKey'=>$extKey, 'severity'=>$severity, 'dataVar'=>$dataVar];
     * 'msg'       string   Message (in english).
     * 'extKey'    string   Extension key (from which extension you are calling the log)
     * 'severity'  integer  Severity: 0 is info, 1 is notice, 2 is warning, 3 is fatal error, -1 is "OK" message
     * 'dataVar'   array    Additional data you want to pass to the logger.
     *
     * @param array $logArr: log data array
     * @return void
     */
    public function devLog($logArr)
    {
        if (
            strpos($logArr['msg'], 'logDeprecatedFunction') !== false
            && strpos($logArr['msg'], 'ApacheSolrForTypo3\Solrfal') !== false
        ) {
            $GLOBALS['T3_VAR']['ext']['solrfal']['deprecationLog'][] = $logArr['msg'];
        }
    }
}
