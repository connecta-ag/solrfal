<?php
namespace ApacheSolrForTypo3\Solrfal\Tests\Integration\Indexing;

/***************************************************************
 * Copyright notice
 *
 * (c) 2016 Markus Friedrich <markus.friedrich@dkd.de>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use ApacheSolrForTypo3\Solrfal\Queue\ItemRepository;
use ApacheSolrForTypo3\Solrfal\Tests\Integration\IntegrationTest;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Testcase for the ItemRepository class
 *
 * @author Timo Hund <timo.hund@dkd.de>
 * @package TYPO3
 * @subpackage solrfal
 */
class ItemRepositoryTest extends IntegrationTest
{
    /**
     * @test
     */
    public function canGetStatistic()
    {
        $this->importDataSetFromFixture('can_get_statistics.xml');

        /** @var $repository ItemRepository */
        $repository = GeneralUtility::makeInstance(ItemRepository::class);
        $statistic = $repository->getStatisticsByRootPageId(1);

        $this->assertSame($statistic->getTotalCount(), 5, 'Can not get total count');
        $this->assertSame($statistic->getFailedCount(), 2, 'Can not get failed count');
        $this->assertSame($statistic->getPendingCount(), 1, 'Can not get pending count');
        $this->assertSame($statistic->getSuccessCount(), 2, 'Can not get success count');
    }

    /**
     * @test
     */
    public function findAllOutStandingMergeIdSetsIgnoresErroredItems()
    {
        /** @var $repository ItemRepository */
        $repository = GeneralUtility::makeInstance(ItemRepository::class);

        $this->importDataSetFromFixture('can_find_indexing_outstanding_merged_but_not_errored.xml');
        $items = $repository->findAllOutStandingMergeIdSets();
        $this->assertCount(1, $items, 'Solrfals index queue does not recognize errored items and loops infinitely by working on queue.');
    }
}