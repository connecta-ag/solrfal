<?php
namespace ApacheSolrForTypo3\Solrfal\Tests\Integration\Service;

use ApacheSolrForTypo3\Solrfal\Service\FileAttachmentResolver;
use ApacheSolrForTypo3\Solrfal\Service\FileAttachmentResolverAspectInterface;

class TestFileAttachmentResolverProcessor implements FileAttachmentResolverAspectInterface
{

    /**
     * @param array $fileUids
     * @param string $tableName
     * @param string $fieldName
     * @param array $record
     * @param FileAttachmentResolver $fileAttachmentResolver
     * @return array
     */
    public function postDetectFilesInField($fileUids, $tableName, $fieldName, $record, FileAttachmentResolver $fileAttachmentResolver)
    {
        $fileUids[] = 9999;
        return $fileUids;
    }
}
