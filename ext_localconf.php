<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

// Register initializing of the Index Queue for Files
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['solr']['postProcessIndexQueueInitialization']['solrfal'] = \ApacheSolrForTypo3\Solrfal\Queue\InitializationAspect::class;

// Register garbage collection
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['solr']['postProcessGarbageCollector']['fileGarbageCollector'] = \ApacheSolrForTypo3\Solrfal\Queue\ConsistencyAspect::class;

// When it is more easy to instanciate a different queue instance this can be replaced
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects']['ApacheSolrForTypo3\Solr\IndexQueue\Queue'] = [
    'className' => \ApacheSolrForTypo3\Solrfal\Queue\Queue::class
];

/**********************************************
 *
 *  REGISTER Events in the FAL API
 *
 **********************************************/

/** @var \TYPO3\CMS\Extbase\SignalSlot\Dispatcher $signalSlotDispatcher */
$signalSlotDispatcher = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\SignalSlot\Dispatcher::class);

$signalsToRegister = [
    'Purge removed files' => [
        \TYPO3\CMS\Core\Resource\ResourceStorage::class, \TYPO3\CMS\Core\Resource\ResourceStorage::SIGNAL_PostFileDelete,
        \ApacheSolrForTypo3\Solrfal\Queue\ConsistencyAspect::class, 'removeDeletedFile'
    ],
    'File Index Record created' => [
        \TYPO3\CMS\Core\Resource\Index\FileIndexRepository::class, 'recordCreated',
        \ApacheSolrForTypo3\Solrfal\Queue\ConsistencyAspect::class, 'fileIndexRecordCreated'
    ],
    'File Index Record updated' => [
        \TYPO3\CMS\Core\Resource\Index\FileIndexRepository::class, 'recordUpdated',
        \ApacheSolrForTypo3\Solrfal\Queue\ConsistencyAspect::class, 'fileIndexRecordUpdated'
    ],
    'File Index Record deleted' => [
        \TYPO3\CMS\Core\Resource\Index\FileIndexRepository::class, 'recordDeleted',
        \ApacheSolrForTypo3\Solrfal\Queue\ConsistencyAspect::class, 'fileIndexRecordDeleted'
    ],
    'File Index Record marked as missing' => [
        \TYPO3\CMS\Core\Resource\Index\FileIndexRepository::class, 'recordMarkedAsMissing',
        \ApacheSolrForTypo3\Solrfal\Queue\ConsistencyAspect::class, 'removeMissingFile'
    ]
];

foreach ($signalsToRegister as $parameters) {
    $signalSlotDispatcher->connect($parameters[0], $parameters[1], $parameters[2], $parameters[3]);
}

/****************************************
 *
 *  REGISTER Events internally
 *
 ****************************************/

$signalSlotDispatcher->connect(
    \ApacheSolrForTypo3\Solrfal\Queue\ItemRepository::class, 'beforeItemRemoved',
    \ApacheSolrForTypo3\Solrfal\Queue\ConsistencyAspect::class, 'removeSolrEntryForItem'
);

$signalSlotDispatcher->connect(
    \ApacheSolrForTypo3\Solrfal\Queue\ItemRepository::class, 'beforeMultipleItemsRemoved',
    \ApacheSolrForTypo3\Solrfal\Queue\ConsistencyAspect::class, 'removeMultipleQueueItemsFromSolr'
);

$signalSlotDispatcher->connect(
    \ApacheSolrForTypo3\Solr\Domain\Index\IndexService::class, 'afterIndexItem',
    \ApacheSolrForTypo3\Solrfal\Detection\PageContextDetectorFrontendIndexingAspect::class, 'resetSuccessfulFileUids'
);


// adding scheduler tasks
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks']['ApacheSolrForTypo3\Solrfal\Scheduler\IndexingTask'] = [
    'extension'        => $_EXTKEY,
    'title'            => 'LLL:EXT:solrfal/Resources/Private/Language/locallang.xlf:scheduler.title',
    'description'      => 'LLL:EXT:solrfal/Resources/Private/Language/locallang.xlf:scheduler.description',
    'additionalFields' => \ApacheSolrForTypo3\Solrfal\Scheduler\IndexingTaskAdditionalFieldProvider::class
];

if (TYPO3_MODE == 'FE' && isset($_SERVER['HTTP_X_TX_SOLR_IQ'])) {
    // register PageContext stuff
    $signalSlotDispatcher->connect(
        \TYPO3\CMS\Core\Resource\ResourceStorage::class,
        'preGeneratePublicUrl',
        \ApacheSolrForTypo3\Solrfal\Detection\PageContextDetectorFrontendIndexingAspect::class,
        'registerGeneratedPublicUrl'
    );

    $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['solr']['Indexer']['indexPagePostProcessPageDocument'][] = \ApacheSolrForTypo3\Solrfal\Detection\PageContextDetectorFrontendIndexingAspect::class;
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_content.php']['postInit'][] = \ApacheSolrForTypo3\Solrfal\Detection\PageContextDetectorFrontendIndexingAspect::class;
}

if (!isset($GLOBALS['TYPO3_CONF_VARS']['LOG']['ApacheSolrForTypo3']['Solrfal']['writerConfiguration'])) {
    $context = \TYPO3\CMS\Core\Utility\GeneralUtility::getApplicationContext();
    if ($context->isProduction()) {
        $logLevel = \TYPO3\CMS\Core\Log\LogLevel::ERROR;
    } elseif ($context->isDevelopment()) {
        $logLevel = \TYPO3\CMS\Core\Log\LogLevel::DEBUG;
    } else {
        $logLevel = \TYPO3\CMS\Core\Log\LogLevel::INFO;
    }

    $GLOBALS['TYPO3_CONF_VARS']['LOG']['ApacheSolrForTypo3']['Solrfal']['writerConfiguration'] = [
        $logLevel => [
            'TYPO3\\CMS\\Core\\Log\\Writer\\FileWriter' => [
                'logFile' => 'typo3temp/logs/solrfal.log'
            ]
        ],
    ];
}
