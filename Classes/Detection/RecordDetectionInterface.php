<?php
namespace ApacheSolrForTypo3\Solrfal\Detection;

/***************************************************************
 * Copyright notice
 *
 * (c) 2013 Steffen Ritter <steffen.ritter@typo3.org>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use ApacheSolrForTypo3\Solr\Site;

/**
 * Interface RecordDetectionInterface
 */
interface RecordDetectionInterface
{

    /**
     * @param Site $site
     */
    public function __construct(Site $site);

    /**
     * @param array $initializationStatus
     * @return void
     */
    public function initializeQueue(array $initializationStatus);

    /**
     * @param string $table
     * @param integer $uid
     *
     * @return void
     */
    public function recordCreated($table, $uid);

    /**
     * @param string $table
     * @param integer $uid
     *
     * @return void
     */
    public function recordUpdated($table, $uid);

    /**
     * @param string $table
     * @param integer $uid
     *
     * @return void
     */
    public function recordDeleted($table, $uid);

    /**
     * Handles new sys_file entries
     *
     * @param string $table
     * @param integer $uid
     *
     * @return void
     */
    public function fileIndexRecordCreated($table, $uid);

    /**
     * Handles updates on sys_file entries
     *
     * @param string $table
     * @param integer $uid
     *
     * @return void
     */
    public function fileIndexRecordUpdated($table, $uid);

    /**
     * Handles deletions of sys_file entries
     *
     * @param string $table
     * @param integer $uid
     *
     * @return void
     */
    public function fileIndexRecordDeleted($table, $uid);
}
