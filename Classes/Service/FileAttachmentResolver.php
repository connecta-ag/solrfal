<?php
namespace ApacheSolrForTypo3\Solrfal\Service;

/***************************************************************
 * Copyright notice
 *
 * (c) 2014 Steffen Ritter <steffen.ritter@typo3.org>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\LinkHandling\LinkService;
use TYPO3\CMS\Core\Log\LogManager;
use TYPO3\CMS\Core\Resource\Collection\AbstractFileCollection;
use TYPO3\CMS\Core\Resource\Exception\FolderDoesNotExistException;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Resource\FileCollectionRepository;
use TYPO3\CMS\Core\Resource\FileReference;
use TYPO3\CMS\Core\Resource\FileRepository;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\MathUtility;
use TYPO3\CMS\Frontend\Page\PageRepository;

/**
 * Class FileAttachmentResolver
 */
class FileAttachmentResolver implements SingletonInterface
{

    /**
     * Detects all files in a record
     *
     * @param string $tableName
     * @param array $record
     * @param array $restrictToFields
     *
     * @return array key=fieldName, value=array of uids
     */
    public function detectFilesInRecord($tableName, $record, $restrictToFields = [])
    {
        if ($restrictToFields === []) {
            $restrictToFields = array_keys($record);
        }
        $fileUids = [];
        foreach ($restrictToFields as $fieldName) {
            $fileUids[$fieldName] = $this->detectFilesInField($tableName, $fieldName, $record);
        }
        return $fileUids;
    }

    /**
     * Detects attachments of an single field
     *
     * @param string $tableName
     * @param string $fieldName
     * @param array $record
     *
     * @return int[]
     */
    public function detectFilesInField($tableName, $fieldName, $record)
    {
        if (!isset($GLOBALS['TCA'][$tableName]) || !isset($GLOBALS['TCA'][$tableName]['columns'][$fieldName]) || empty($record[$fieldName])) {
            return [];
        }
        $fieldConfiguration = $GLOBALS['TCA'][$tableName]['columns'][$fieldName]['config'];
        $fileUids = [];
        switch ($fieldConfiguration['type']) {
            case 'input':
                // single line and multi line text fields behave the same
            case 'text':
                $fileUids = $this->detectFilesInTextField($fieldName, $record);
                break;
            case 'group':
                $fileUids = $this->detectFilesInGroupField($tableName, $fieldName, $record, $fieldConfiguration);
                break;
            case 'select':
                // todo no use case existent currently
                break;
            case 'inline':
                $fileUids = $this->detectFilesInInlineField($tableName, $fieldName, $record, $fieldConfiguration);
                break;
            case 'flex':
                // todo: files in flexforms are not supported yet
                break;
            default:
                break;
        }

        $fileUids = $this->applyPostDetectFilesInFieldHook($fileUids, $tableName, $fieldName, $record);
        return $fileUids;
    }

    /**
     * Calls postDetectFilesInField on the configured FileAttachmentResolverAspects:
     *
     * $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['solrfal']['FileAttachmentResolverAspect']
     *
     * @param string $tableName
     * @param string $fieldName
     * @param array $record
     *
     * @return int[]
     */
    protected function applyPostDetectFilesInFieldHook($fileUids, $tableName, $fieldName, $record)
    {
        $fileAttachmentResolverAspects = $this->getFileAttachmentResolverAspects();
        if (count($fileAttachmentResolverAspects) == 0) {
            return $fileUids;
        }

        // we have valid hooks and trigger them
        foreach ($fileAttachmentResolverAspects as $fileAttachmentResolverAspect) {
            $fileUids = $fileAttachmentResolverAspect->postDetectFilesInField($fileUids, $tableName, $fieldName, $record, $this);
        }

        // check files
        $fileUids = $this->checkFiles($fileUids);

        return $fileUids;
    }

    /**
     * Returns the array of the registered class references of the ResolverAspects or an empty array, when non
     * is registered.
     *
     * @throws \Exception
     * @return FileAttachmentResolverAspectInterface[]
     */
    protected function getFileAttachmentResolverAspects()
    {
        $hasFileAttachmentResolverProcessor = !empty($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['solrfal']) && is_array($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['solrfal']['FileAttachmentResolverAspect']);
        if (!$hasFileAttachmentResolverProcessor) {
            return [];
        }

        $result = [];
        $fileAttachmentResolverAspectReferences = $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['solrfal']['FileAttachmentResolverAspect'];

        foreach ($fileAttachmentResolverAspectReferences as $fileAttachmentResolverAspectReference) {
            $fileAttachmentResolverAspect = GeneralUtility::getUserObj($fileAttachmentResolverAspectReference);
            if (!$fileAttachmentResolverAspect instanceof FileAttachmentResolverAspectInterface) {
                throw new \Exception('Invalid hook definition for FileAttachmentResolverAspect');
            }
            $result[] = $fileAttachmentResolverAspect;
        }

        return $result;
    }

    /**
     * Extracts from text fields (text/input)
     *
     * @param string $fieldName
     * @param array $record
     * @return int[]
     */
    protected function detectFilesInTextField($fieldName, array $record)
    {
        $fileUids = [];
        $value = $record[$fieldName];
        // link field with link-browser links like "file:uid"
        if (substr(trim($value), 0, 5) === 'file:') {
            $valueString = substr(trim($value), 5) . ' ';
            $valueString = substr($valueString, 0, strpos($valueString, ' '));
            if (MathUtility::canBeInterpretedAsInteger($valueString)) {
                $fileUids[] = (int)$valueString;
            }
            // fulltext field, a.k.a RTE
        } else {
            $fileUidFromLinkTagLinks = $this->getFileUidsFromReferencedFilesWithLinkSyntax($value);
            $fileUids = array_merge($fileUids, $fileUidFromLinkTagLinks);

            // Only when the LinkService is present we need to parse the t3:// links
            // when 8 LTS is required the check for LinkService can be removed
            if (class_exists('TYPO3\\CMS\\Core\\LinkHandling\\LinkService')) {
                $fileUidFromT3Links = $this->getFileUidsFromReferencedFilesWithT3Syntax($value);
                $fileUids = array_merge($fileUids, $fileUidFromT3Links);
            }
        }

        // check files
        $fileUids = $this->checkFiles($fileUids);

        return $fileUids;
    }

    /**
     * Retrieves referenced file uid's with the syntax file:..
     *
     * @todo This method parsed the old syntax <link file:...> when the 7.6 compatibility is dropped this method can be dropped as well.
     * @param string $value
     * @return array
     */
    protected function getFileUidsFromReferencedFilesWithLinkSyntax($value)
    {
        $fileUids = [];
        $results = [];
        preg_match_all('/\<link\s+file:(?P<file>\d+)(\s|\>)/i', $value, $results);
        if (isset($results['file'])) {
            foreach ((array)$results['file'] as $foundFile) {
                $fileUids[] = $foundFile;
            }
        }
        return $fileUids;
    }

    /**
     * Retrieves referenced files uid's with the new reference syntax t3://file...
     *
     * @param string $value
     * @return array
     */
    protected function getFileUidsFromReferencedFilesWithT3Syntax($value)
    {
        $fileUids = [];

        $regexResult = [];
        preg_match_all('/(?<file>t3:\/\/file?[^>"]*)/i', $value, $regexResult);

            /** @var $linkService \TYPO3\CMS\Core\LinkHandling\LinkService */
        $linkService = GeneralUtility::makeInstance(LinkService::class);
        foreach ($regexResult['file'] as $fileLink) {
            $link = $linkService->resolve($fileLink);

            if (!isset($link['file'])) {
                continue;
            }
                /** @var $file File */
            $file = $link['file'];
            $fileUids[] = $file->getUid();
        }

        return $fileUids;
    }

    /**
     * Extracts from Group field
     *
     * @param string $tableName
     * @param string $fieldName
     * @param array $record
     * @param array $fieldConfiguration
     *
     * @return int[]
     */
    protected function detectFilesInGroupField($tableName, $fieldName, array $record, array $fieldConfiguration)
    {
        $values = GeneralUtility::trimExplode(',', $record[$fieldName]);
        if ($values === []) {
            return [];
        }
        $internalType = $fieldConfiguration['internal_type'];
        $fileUids = [];
        switch ($internalType) {
            case 'db':
                if ($fieldConfiguration['allowed'] === '*'
                    || GeneralUtility::inList($fieldConfiguration['allowed'], 'sys_file')
                    || GeneralUtility::inList($fieldConfiguration['allowed'], 'sys_file_collection')
                ) {
                    if (isset($fieldConfiguration['MM'])) {
                        if ($fieldConfiguration['MM'] === 'sys_file_reference') {
                            $repository = GeneralUtility::makeInstance(FileRepository::class);
                            /** @var \TYPO3\CMS\Core\Resource\FileReference[] $fileReferences */
                            $fileReferences = $repository->findByRelation($tableName, $fieldName, $record['uid']);
                            foreach ($fileReferences as $fileReference) {
                                if (!$this->isValidFileReference($fileReference)) {
                                    continue;
                                }

                                $fileUids[] = $fileReference->getOriginalFile()->getUid();
                            }
                        }
                    } else {
                        foreach ($values as $value) {
                            list($table, $uid) = BackendUtility::splitTable_Uid($value);
                            if ((empty($table) && $fieldConfiguration['allowed'] == 'sys_file') || $table == 'sys_file') {
                                $fileUids[] = intval($uid);
                            } elseif ((empty($table) && $fieldConfiguration['allowed'] == 'sys_file_collection') || $table == 'sys_file_collection') {
                                $this->addFileUidsFromCollectionToArray($uid, $fileUids);
                            }
                        }
                    }
                }
                break;
            case 'file_reference':
                foreach ($values as $fileReference) {
                    // that's safe since 'file_reference' only works locally with fileadmin/
                    if (file_exists(PATH_site . $fileReference)) {
                        $fileObject = ResourceFactory::getInstance()->getFileObjectFromCombinedIdentifier($fileReference);
                        $fileUids[] = $fileObject->getUid();
                    }
                }
                break;
            case 'file':
                // solrfal does not support non FAL fields
                break;
            case 'folder':
                foreach ($values as $folderPath) {
                    try {
                        $folderObject = ResourceFactory::getInstance()->getFolderObjectFromCombinedIdentifier($folderPath);
                        foreach ($folderObject->getFiles() as $fileObject) {
                            $fileUids[] = $fileObject->getUid();
                        }
                    } catch (FolderDoesNotExistException $e) {
                        continue;
                    }
                }
                break;
            default:
        }
        return $fileUids;
    }

    /**
     * Adds the UIDs of the files found in the collection with the given
     * $collectionUid to the $fileUidArray.
     *
     * @param int $collectionUid The UID of the collection
     * @param array $fileUidArray The array to which the file UIDs will be added.
     */
    protected function addFileUidsFromCollectionToArray($collectionUid, array &$fileUidArray)
    {
        $collectionRepository = GeneralUtility::makeInstance(FileCollectionRepository::class);
        $fileCollection = $collectionRepository->findByUid($collectionUid);

        if ($fileCollection instanceof AbstractFileCollection) {
            $fileCollection->loadContents();
            /** @var \TYPO3\CMS\Core\Resource\File $file */
            foreach ($fileCollection->getItems() as $file) {
                $fileUidArray[] = $file->getUid();
            }
        }
    }

    /**
     * Extracts from inline fields
     *
     * @param string $tableName
     * @param string $fieldName
     * @param array $record
     * @param array $fieldConfiguration
     * @return int[]
     */
    protected function detectFilesInInlineField($tableName, $fieldName, array $record, array $fieldConfiguration)
    {
        $fileUids = [];

        if ($fieldConfiguration['foreign_table'] === 'sys_file_reference') {
            // it's save to use the PageRepository method without initialization,
            // since it does not access any object properties.
            $repository = GeneralUtility::makeInstance(PageRepository::class);
            $fileReferences = $repository->getFileReferences($tableName, $fieldName, $record);
            foreach ($fileReferences as $fileReference) {
                $fileReference = $this->getUpdatedFileReference($fileReference);

                if (!$this->isValidFileReference($fileReference)) {
                    continue;
                }

                $fileUids[] = $fileReference->getOriginalFile()->getUid();
            }
        }

        return $fileUids;
    }

    /**
     * Get updated and uncached file reference
     *
     * We do this since the ResourceFactory caches the
     * file reference objects and we cannot be sure that this
     * object is up-to-date
     *
     * @param \TYPO3\CMS\Core\Resource\FileReference $fileReference
     * @return \TYPO3\CMS\Core\Resource\FileReference $fileReference
     */
    protected function getUpdatedFileReference(FileReference $fileReference)
    {
        try {
            $fileReferenceData = BackendUtility::getRecord('sys_file_reference', $fileReference->getUid());
            $fileReference = ResourceFactory::getInstance()->createFileReferenceObject($fileReferenceData);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }

        return $fileReference;
    }

    /**
     * Checks if file reference is valid and may be added to the index queue
     *
     * @param \TYPO3\CMS\Core\Resource\FileReference $fileReference
     * @return boolean
     */
    protected function isValidFileReference(FileReference $fileReference)
    {
        return !($fileReference->getReferenceProperty('hidden') || $fileReference->isMissing());
    }

    /**
     * Checks if files are valid and removes invalid files that may not be added to the index queue
     *
     * @param array $fileUids
     * @return array
     */
    protected function checkFiles(array $fileUids)
    {
        $checkedFileUids = [];

        foreach ($fileUids as $fileUid) {
            try {
                $file = ResourceFactory::getInstance()->getFileObject((int)$fileUid);
                if (
                    !$file->isMissing()
                    && !$file->isDeleted()
                    && $file->exists()
                ) {
                    $checkedFileUids[] = $file->getUid();
                }
            } catch (\Exception $e) {
                $logger = GeneralUtility::makeInstance(LogManager::class)->getLogger(__CLASS__);
                $logger->error('File not found: ' . $fileUid);
            }
        }

        return $checkedFileUids;
    }
}
