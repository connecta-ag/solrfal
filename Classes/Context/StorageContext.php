<?php
namespace ApacheSolrForTypo3\Solrfal\Context;

/***************************************************************
 * Copyright notice
 *
 * (c) 2013 Steffen Ritter <steffen.ritter@typo3.org>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use ApacheSolrForTypo3\Solr\Util;
use TYPO3\CMS\Core\Resource\File;

/**
 * Class StorageContext
 */
class StorageContext extends AbstractContext
{

    /**
     * @return string
     */
    public function getContextIdentifier()
    {
        return 'storage';
    }

    public function getAdditionalDynamicDocumentFields(File $file)
    {
        $fields = [];
        $storageConfiguration = Util::getSolrConfigurationFromPageId(
            $this->getSite()->getRootPageId(),
            false,
            $this->getLanguage()
        );
        $enableFields = $storageConfiguration->getObjectByPathOrDefault('plugin.tx_solr.index.enableFileIndexing.storageContext.' . $this->getIdentifierForItemSpecificFieldConfiguration() . '.enableFields.', []);
        foreach ($enableFields as $identifier => $fieldName) {
            switch ($identifier) {
                case 'endtime':
                    if ((int)$file->getProperty($fieldName) !== 0) {
                        $fields['endtime'] = (int)$file->getProperty($fieldName);
                    }
                    break;
                case 'accessGroups':
                    if (trim($file->getProperty($fieldName))) {
                        $fields['access'] = 'r:' . trim($file->getProperty($fieldName));
                    } else {
                        $fields['access'] = 'r:0';
                    }
                    break;
                default:
            }
        }
        return $fields;
    }

    /**
     * Returns an identifier, which will be used for looking up special
     * configurations in TypoScript like storage uid in storageContext
     * or table name in recordContext
     *
     * @return string
     */
    protected function getIdentifierForItemSpecificFieldConfiguration()
    {
        return (int)$this->additionalDocumentFields['fileStorage'];
    }
}
