<?php
namespace ApacheSolrForTypo3\Solrfal\Context;

/***************************************************************
 * Copyright notice
 *
 * (c) 2013 Steffen Ritter <steffen.ritter@typo3.org>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use ApacheSolrForTypo3\Solr\Access\Rootline;
use ApacheSolrForTypo3\Solr\Site;
use ApacheSolrForTypo3\Solr\Util;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\Page\PageRepository;

/**
 * Class StorageContext
 *
 */
class PageContext extends RecordContext
{

    /**
     * @var integer
     */
    protected $pageId;


    /**
     * @return string
     */
    public function getContextIdentifier()
    {
        return 'page';
    }

    /**
     * @return integer
     */
    public function getPageId()
    {
        return $this->pageId;
    }


    /**
     * @param Site $site
     * @param Rootline $accessRestrictions
     * @param integer $pageUid
     * @param string $table
     * @param string $field
     * @param integer $uid
     * @param integer $language
     */
    public function __construct(Site $site, Rootline $accessRestrictions, $pageUid, $table, $field, $uid, $language = 0)
    {
        parent::__construct($site, $accessRestrictions, $table, $field, $uid, 'pages', $language);
        $this->pageId = $pageUid;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return array_merge(
            parent::toArray(),
            [
                'context_record_page' => $this->getPageId()
            ]
        );
    }

    /**
     * Returns an array of context specific field to add to the solr document
     *
     * @return array
     */
    public function getAdditionalStaticDocumentFields()
    {
        return array_merge(
            parent::getAdditionalStaticDocumentFields(),
            [
                'fileReferenceUrl' => $this->getPageId()
            ]
        );
    }

    /**
     * @param File $file
     *
     * @return array
     */
    public function getAdditionalDynamicDocumentFields(File $file)
    {
        $dynamicFields = [];

        $pageRepository = $this->getPageRepository();
        $pageRow = $pageRepository->getPage((int)$this->getPageId());

        if (!empty($pageRow)) {
            $dynamicFields['fileReferenceTitle'] = $pageRow['title'];

            $pageContextConfiguration = Util::getSolrConfigurationFromPageId(
                $this->getSite()->getRootPageId(),
                false,
                $this->getLanguage()
            );

            $enableFields = $pageContextConfiguration->getObjectByPathOrDefault('plugin.tx_solr.index.enableFileIndexing.pageContext.enableFields.', []);
            foreach ($enableFields as $identifier => $fieldName) {
                switch ($identifier) {
                    case 'endtime':
                        if ((int)$pageRow[$fieldName] !== 0) {
                            $dynamicFields['endtime'] = (int)$pageRow[$fieldName];
                        }
                        break;
                    //case 'accessGroups': todo: why commented
                    //	if (trim($pageRow[$fieldName])) {
                    //		$dynamicFields['access'] = 'c:' . trim($pageRow[$fieldName]);
                    //	}
                    //	break;
                    default:
                }
            }
        }
        return $dynamicFields;
    }

    /**
     * Resolves the field-processing TypoScript configuration which is specific
     * to the current context.
     * Will be merged in the default field-processing configuration and takes
     * precedence over the default configuration.
     *
     * @return array
     */
    public function getSpecificFieldConfigurationTypoScript()
    {
        $fileConfiguration = Util::getSolrConfigurationFromPageId(
            $this->getSite()->getRootPageId(),
            false,
            $this->getLanguage()
        );
        return $fileConfiguration->getObjectByPathOrDefault('plugin.tx_solr.index.queue._FILES.' . $this->getContextIdentifier() . 'Context.', []);
    }

    /**
     * @return \TYPO3\CMS\Core\Database\DatabaseConnection
     */
    protected function getDatabaseConnection()
    {
        return $GLOBALS['TYPO3_DB'];
    }

    /**
     * Returns the page repository
     *
     * @return \TYPO3\CMS\Frontend\Page\PageRepository
     */
    protected function getPageRepository()
    {
        Util::initializeTsfe($this->getPageId(), $this->getLanguage());
        $pageRepository = GeneralUtility::makeInstance(PageRepository::class);
        $pageRepository->sys_language_uid = $this->language;
        return $pageRepository;
    }
}
