<?php
namespace ApacheSolrForTypo3\Solrfal\Context;

/***************************************************************
 * Copyright notice
 *
 * (c) 2013 Steffen Ritter <steffen.ritter@typo3.org>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use ApacheSolrForTypo3\Solr\Access\Rootline;
use ApacheSolrForTypo3\Solr\Site;
use ApacheSolrForTypo3\Solr\Util;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Utility\ArrayUtility;

/**
 * Class Context
 */
abstract class AbstractContext implements ContextInterface
{

    /**
     * @var Site
     */
    protected $site;

    /**
     * @var Rootline
     */
    protected $accessRestrictions = null;

    /**
     * @var integer
     */
    protected $language = 0;

    /**
     * @var array
     */
    protected $additionalDocumentFields = [];

    /**
     * @var string
     */
    protected $indexingConfiguration = '';

    /**
     * @var boolean
     */
    protected $error = false;

    /**
     * @var string
     */
    protected $errorMessage = '';

    /**
     * @param Site $site
     * @param Rootline $accessRestrictions
     * @param integer $language
     * @param string $indexingConfiguration
     */
    public function __construct(Site $site, Rootline $accessRestrictions, $language = 0, $indexingConfiguration = '')
    {
        $this->site               = $site;
        $this->accessRestrictions = $accessRestrictions;
        $this->language           = $language;
        $this->indexingConfiguration = $indexingConfiguration;
    }

    /**
     * @return string
     */
    protected function getIndexingConfiguration()
    {
        return $this->indexingConfiguration;
    }

    /**
     * @return boolean
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * @return Rootline
     */
    public function getAccessRestrictions()
    {
        return $this->accessRestrictions;
    }

    /**
     * @return int
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @return Site
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Returns the array representation for database storage
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'context_type'                => $this->getContextIdentifier(),
            'context_language'            => $this->getLanguage(),
            'context_access_restrictions' => $this->getAccessRestrictions()->__toString(),
            'context_site'                => (int) $this->getSite()->getRootPageId(),
            'context_additional_fields'   => json_encode($this->additionalDocumentFields),
            'context_record_indexing_configuration' => $this->getIndexingConfiguration(),
            'error'                       => (int) $this->getError(),
            'error_message'               => $this->getErrorMessage()
        ];
    }

    /**
     * Returns the pageId of this context
     *
     * @return int
     */
    public function getPageId()
    {
        return $this->getSite()->getRootPageId();
    }

    /**
     * @param array $additionalDocumentFields
     * @return void
     */
    public function setAdditionalDocumentFields(array $additionalDocumentFields)
    {
        $this->additionalDocumentFields = $additionalDocumentFields;
    }

    /**
     * @return array
     */
    public function getAdditionalStaticDocumentFields()
    {
        return $this->additionalDocumentFields;
    }

    /**
     * Returns an array of context specific field to add to the solr document,
     * dynamically calculated from the FILE
     *
     * @param File $file
     *
     * @return array
     */
    public function getAdditionalDynamicDocumentFields(File $file)
    {
        return [];
    }

    /**
     * Resolves the field-processing TypoScript configuration which is specific
     * to the current context.
     * Will be merged in the default field-processing configuration and takes
     * precedence over the default configuration.
     *
     * @return array
     */
    public function getSpecificFieldConfigurationTypoScript()
    {
        $fileConfiguration = Util::getSolrConfigurationFromPageId(
            $this->getSite()->getRootPageId(),
            false,
            $this->getLanguage()
        );

        $contextConfiguration = $fileConfiguration->getObjectByPathOrDefault('plugin.tx_solr.index.queue._FILES.' . $this->getContextIdentifier() . 'Context.', []);
        $configurationArray = [];

        if (array_key_exists('default.', $contextConfiguration) && is_array($contextConfiguration['default.'])) {
            $configurationArray = $contextConfiguration['default.'];
        }

        $itemSpecificConfigKey = $this->getIdentifierForItemSpecificFieldConfiguration() . '.';
        if (array_key_exists($itemSpecificConfigKey, $contextConfiguration) && is_array($contextConfiguration[$itemSpecificConfigKey])) {
            ArrayUtility::mergeRecursiveWithOverrule(
                $configurationArray,
                $contextConfiguration[$itemSpecificConfigKey]
            );
        }
        return $configurationArray;
    }

    /**
     * Returns an identifier, which will be used for looking up special
     * configurations in TypoScript like storage uid in storageContext
     * or table name in recordContext
     *
     * @return string
     */
    abstract protected function getIdentifierForItemSpecificFieldConfiguration();
}
