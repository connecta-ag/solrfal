<?php
namespace ApacheSolrForTypo3\Solrfal\Queue;

/***************************************************************
 * Copyright notice
 *
 * (c) 2013 Steffen Ritter <steffen.ritter@typo3.org>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use ApacheSolrForTypo3\Solr\Domain\Index\Queue\Statistic\QueueStatistic;
use ApacheSolrForTypo3\Solr\Site;
use ApacheSolrForTypo3\Solrfal\Context\ContextFactory;
use ApacheSolrForTypo3\Solrfal\Context\RecordContext;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\SignalSlot\Dispatcher;

/**
 * Class PersistenceManager
 */
class ItemRepository
{

    /**
     * @var Item[]
     */
    protected static $identityMap = [];

    /**
     * @var string
     */
    protected $tableName = 'tx_solr_indexqueue_file';

    /**
     * @var string[]
     */
    protected $fields = [
        'uid', 'last_update', 'last_indexed', 'file', 'context_type', 'context_site', 'context_access_restrictions', 'context_language',
        'context_record_uid', 'context_record_table', 'context_record_field', 'context_record_page', 'context_record_indexing_configuration',
        'context_additional_fields', 'error', 'error_message', 'merge_id'
    ];

    /*++++++++++++++++++++++++++++++*
     *                              *
     *       Getting Objects        *
     *                              *
     *++++++++++++++++++++++++++++++*/

    /**
     * Finds queue item by uid
     *
     * @param integer $uid
     * @return Item
     */
    public function findByUid($uid)
    {
        $records = $this->fetchRecordsFromDatabase('uid = ' . (int) $uid);
        if (!empty($records)) {
            $item = $this->createObject($records[0]);
        } else {
            $item = null;
        }

        return $item;
    }

    /**
     * Finds all queue items
     *
     * @return Item[]
     */
    public function findAll()
    {
        $records = $this->fetchRecordsFromDatabase();

        return $this->createObjectsFromRowArray($records);
    }

    /**
     * Finds queue item by file uid
     *
     * @param integer $fileUid
     * @return Item[]
     */
    public function findByFileUid($fileUid)
    {
        return $this->createObjectsFromRowArray(
            $this->fetchRecordsFromDatabase('file = ' . (int) $fileUid)
        );
    }

    /**
     * Finds queue item by file uid
     *
     * @param File $file
     * @return Item[]
     */
    public function findByFile(File $file)
    {
        return $this->findByFileUid($file->getUid());
    }

    /**
     * @param $itemCountLimit
     *
     * @return Item[]
     */
    public function findAllIndexingOutStanding($itemCountLimit = -1)
    {
        return $this->createObjectsFromRowArray(
            $this->fetchRecordsFromDatabase('last_update > last_indexed', $itemCountLimit == -1 ? '' : $itemCountLimit)
        );
    }

    /**
     * @param $itemCountLimit
     *
     * @return Item[]
     */
    public function findAllOutStandingMergeIdSets($itemCountLimit = -1)
    {
        $limit = $itemCountLimit == -1 ? '' : $itemCountLimit;
        $mergeIds = $this->getDatabaseConnection()->exec_SELECTgetRows(
            'merge_id',
            $this->tableName,
            'last_update > last_indexed AND error = 0',
            'merge_id',
            '',
            $limit
        );
        $rows = [];
        foreach ($mergeIds as $mergeId) {
            $rows[] = $mergeId['merge_id'];
       }

        return $rows;
    }


    /**
     * @param string $mergeId
     *
     * @return Item[]
     */
    public function findAllByMergeId($mergeId)
    {
        $mergeId = $this->getDatabaseConnection()->fullQuoteStr($mergeId, $this->tableName);
        return $this->createObjectsFromRowArray(
            $this->fetchRecordsFromDatabase('merge_id = ' . $mergeId)
        );
    }

    /**
     * @return void
     */
    public function flushErrorsBySite(Site $contextSite)
    {
        $this->getDatabaseConnection()->exec_UPDATEquery(
            $this->tableName,
            'context_site = '.(int)$contextSite->getRootPageId(),
            ['error' => 0, 'error_message' => null]
        );
    }

    /**
     * @return void
     */
    public function flushAllErrors()
    {
        $this->getDatabaseConnection()->exec_UPDATEquery(
            $this->tableName,
            '1 = 1',
            ['error' => 0, 'error_message' => null]
        );
    }

    /*++++++++++++++++++++++++++++++*
     *                              *
     *      Counting Objects        *
     *                              *
     *++++++++++++++++++++++++++++++*/

    /**
     * Counts all available Queue\Items
     *
     * @return integer
     */
    public function count()
    {
        return $this->getDatabaseConnection()->exec_SELECTcountRows('uid', $this->tableName);
    }

    /**
     * @param Site $site
     * @param string $indexingConfiguration
     * @deprecated since 4.1 will be removed in 4.2 use getStatisticsBySite()->getTotalCount() please
     * @return mixed
     */
    public function countBySiteAndIndexConfigurationName(Site $site, $indexingConfiguration)
    {
        GeneralUtility::logDeprecatedFunction();

        return $this->getStatisticsBySite($site, $indexingConfiguration)->getTotalCount();
    }

    /**
     * Extracts the number of pending, indexed and erroneous items from the
     * Index Queue.
     *
     * @param Site $site
     * @param string $indexingConfigurationName
     *
     * @return QueueStatistic
     */
    public function getStatisticsBySite(Site $site, $indexingConfigurationName = '')
    {
        return $this->getStatisticsByRootPageId($site->getRootPageId(), $indexingConfigurationName);
    }

    /**
     * Retrieves the statistic for a site by a given rootPageId.
     *
     * @param integer $rootPageId
     * @param string $indexingConfigurationName
     *
     * @return QueueStatistic
     */
    public function getStatisticsByRootPageId($rootPageId, $indexingConfigurationName = '')
    {
        $indexingConfigurationConstraint = $this->buildIndexConfigurationConstraint($indexingConfigurationName);
        $where = 'context_site = ' . (int)$rootPageId . $indexingConfigurationConstraint;

        $indexQueueStats = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
            '(last_indexed < last_update) as pending,'
            .'(error = 1) as failed,'
            . 'COUNT(*) as count',
            $this->tableName,
            $where,
            'pending, failed'
        );

        return $this->buildStatisticsObjectFromRows($indexQueueStats);
    }

    /**
     * Builds a statistics object from the statistic queue database rows.
     *
     * @param array $indexQueueStats
     * @return QueueStatistic
     */
    protected function buildStatisticsObjectFromRows($indexQueueStats)
    {
        /** @var $statistic QueueStatistic */
        $statistic = GeneralUtility::makeInstance(QueueStatistic::class);

        if (!is_array($indexQueueStats)) {
            return $statistic;
        }

        $failed = $pending = $success = 0;

        foreach ($indexQueueStats as $row) {
            if ($row['failed'] == 1) {
                $failed += (int)$row['count'];
            } elseif ($row['pending'] == 1) {
                $pending += (int)$row['count'];
            } else {
                $success += (int)$row['count'];
            }
        }

        $statistic->setFailedCount($failed);
        $statistic->setPendingCount($pending);
        $statistic->setSuccessCount($success);

        return $statistic;
    }

    /**
     * Build a database constraint that limits to a certain indexConfigurationName
     *
     * @param string $indexingConfigurationName
     * @return string
     */
    protected function buildIndexConfigurationConstraint($indexingConfigurationName)
    {
        $indexingConfigurationConstraint = '';
        if (!empty($indexingConfigurationName)) {
            $indexingConfigurationConstraint = ' AND context_record_indexing_configuration = \'' . $indexingConfigurationName . '\'';
            return $indexingConfigurationConstraint;
        }
        return $indexingConfigurationConstraint;
    }

    /**
     * Counts all Queue\Items which need to updated in Solr
     *
     * @return integer
     */
    public function countIndexingOutstanding()
    {
        return $this->getDatabaseConnection()->exec_SELECTcountRows('uid', $this->tableName, 'last_update > last_indexed AND error = 0');
    }

    /**
     * Returns the count of Queue\Items failed to send to solr.
     *
     * @return integer
     */
    public function countFailures()
    {
        return $this->getDatabaseConnection()->exec_SELECTcountRows('uid', $this->tableName, 'error = 1');
    }

    /**
     * Checks whether a item with same information (context and file) already is in index queue
     *
     * @param Item $item
     *
     * @return bool
     */
    public function exists(Item $item)
    {
        $data = [];
        $data['file'] = $item->getFile()->getUid();
        $data['last_update'] = $GLOBALS['EXEC_TIME'] ?: time();
        $data = array_merge($data, $item->getContext()->toArray());

        $data = array_intersect_key($data, array_flip($this->fields));
        $whereClause = $this->getWhereClauseForItemData($data);

        return $this->getDatabaseConnection()->exec_SELECTcountRows('uid', $this->tableName, $whereClause) > 0;
    }

    /*++++++++++++++++++++++++++++++*
     *                              *
     *        Adding Objects        *
     *                              *
     *++++++++++++++++++++++++++++++*/

    /**
     * @param Item $item
     *
     * @return void
     */
    public function add(Item $item)
    {
        $data = [];
        $data['file'] = $item->getFile()->getUid();
        $data['last_update'] = $GLOBALS['EXEC_TIME'] ?: time();
        $data['merge_id'] = $item->getMergeId();
        $data = array_merge($data, $item->getContext()->toArray());

        $data = array_intersect_key($data, array_flip($this->fields));

        $this->getDatabaseConnection()->exec_INSERTquery(
            $this->tableName,
            $data
        );
        // todo - no identity map update
    }

    /*++++++++++++++++++++++++++++++*
     *                              *
     *       Updating Objects       *
     *                              *
     *++++++++++++++++++++++++++++++*/

    /**
     * @param Item $item
     *
     * @return void
     */
    public function update(Item $item)
    {
        $data = [];
        $data['file'] = $item->getFile()->getUid();
        $data['last_update'] = $GLOBALS['EXEC_TIME'] ?: time();
        $data['merge_id'] = $item->getMergeId();

        $data = array_merge($data, $item->getContext()->toArray());

        $data = array_intersect_key($data, array_flip($this->fields));

        $this->getDatabaseConnection()->exec_UPDATEquery(
            $this->tableName,
            $this->getWhereClauseForItemData($data),
            $data
        );
    }

    /**
     * @param Item $item
     * @param string $errorMessage
     *
     * @return void
     */
    public function markFailed(Item $item, $errorMessage = '')
    {
        $item->setError(true);
        $this->getDatabaseConnection()->exec_UPDATEquery(
            $this->tableName,
            'uid = ' . intval($item->getUid()),
            ['error' => 1, 'error_message' => $this->getDatabaseConnection()->fullQuoteStr($errorMessage, $this->tableName)]
        );
    }

    /**
     * @param Item $item
     *
     * @return void
     */
    public function markIndexedSuccessfully(Item $item)
    {
        $this->markMultipleIndexedSuccessfully([$item]);
    }

    /**
     * @param Item[] $items
     *
     * @return void
     */
    public function markMultipleIndexedSuccessfully(array $items)
    {
        $uids = [];
        $executionTime = $GLOBALS['EXEC_TIME'] ?: time();
        foreach ($items as $item) {
            $item->setError(false);
            $item->setLastIndexed($executionTime);
            $uids[] = intval($item->getUid());
        }
        $this->getDatabaseConnection()->exec_UPDATEquery(
            $this->tableName,
            'uid IN (' . implode(',', $uids) . ')',
            [
                'error' => false,
                'error_message' => '',
                'last_indexed' => $executionTime
            ]
        );
    }

    /**
     * @param int $fileUid
     * @param array $contextFilter array with key = field, value => field value to ocmbine to where clause
     *
     * @return void
     */
    public function markFileUpdated($fileUid, array $contextFilter = [])
    {
        $executionTime = $GLOBALS['EXEC_TIME'] ?: time();

        $contextFilter = array_intersect_key($contextFilter, array_flip($this->fields));
        $additionalWhereClause = '';
        foreach ($contextFilter as $field => $desiredValue) {
            $additionalWhereClause .= ' AND ' . $field . ' = ' .
                $this->getDatabaseConnection()->fullQuoteStr($desiredValue, $this->tableName);
        }

        $this->getDatabaseConnection()->exec_UPDATEquery(
            $this->tableName,
            'file = ' . (int)$fileUid . $additionalWhereClause,
            [
                'error' => false,
                'error_message' => '',
                'last_update' => $executionTime
            ]
        );
    }

    /*++++++++++++++++++++++++++++++*
     *                              *
     *      Removing Objects        *
     *                              *
     *++++++++++++++++++++++++++++++*/

    /**
     * Removes an Item from the queue
     *
     * @param Item $item
     *
     * @return void
     */
    public function remove(Item $item)
    {
        $this->emitBeforeItemRemovedFromQueue($item);

        $this->getDatabaseConnection()->exec_DELETEquery(
            $this->tableName,
            'uid = ' . (int)$item->getUid()
        );

        $this->emitItemRemovedFromQueue($item);
        if (array_key_exists($item->getUid(), self::$identityMap)) {
            unset(self::$identityMap[$item->getUid()]);
        }
    }

    /**
     * @param File $file
     *
     * @return void
     */
    public function removeByFile(File $file)
    {
        $this->removeByFileUid($file->getUid());
    }

    /**
     * Removes an file index queue entry
     *
     * @param integer $fileUid
     *
     * @return void
     */
    public function removeByFileUid($fileUid)
    {
        $whereClause = 'file = ' . (int) $fileUid;
        $this->removeByWhereClause($whereClause);
    }

    /**
     * Removes all entries of a certain site from the File Index Queue.
     *
     * @param Site $site The site to remove items for.
     *
     * @return void
     */
    public function removeBySite(Site $site)
    {
        $whereClause = 'context_site = ' . (int)$site->getRootPageId();

        $this->removeByWhereClause($whereClause);
    }

    /**
     * Removes all entries of a certain site from the File Index Queue.
     *
     * @param Site $site The site to remove items for.
     * @param string $contextType
     *
     * @return void
     */
    public function removeBySiteAndContext(Site $site, $contextType)
    {
        $whereClause = 'context_site = ' . (int)$site->getRootPageId() . ' AND context_type = ' .
            $this->getDatabaseConnection()->fullQuoteStr($contextType, $this->tableName);

        $this->removeByWhereClause($whereClause);
    }


    /**
     * Removes all entries of a certain site from the File Index Queue.
     *
     * @param string $contextType
     *
     * @return void
     */
    public function purgeContext($contextType)
    {
        $whereClause = ' context_type = ' .
            $this->getDatabaseConnection()->fullQuoteStr($contextType, $this->tableName);

        $this->removeByWhereClause($whereClause);
    }

    /**
     * @param Site $site
     * @param string $tableName
     *
     * @return void
     */
    public function removeByTableInRecordContext(Site $site, $tableName)
    {
        $whereClause = 'context_site = ' . (int)$site->getRootPageId() . ' AND context_type = "record" AND context_record_table = ' .
            $this->getDatabaseConnection()->fullQuoteStr($tableName, $this->tableName);

        $this->removeByWhereClause($whereClause);
    }

    /**
     * @param Site $site
     * @param string $indexingConfiguration
     *
     * @return void
     */
    public function removeByIndexingConfigurationInRecordContext(Site $site, $indexingConfiguration)
    {
        $whereClause = 'context_site = ' . (int)$site->getRootPageId() . ' AND context_type = "record" AND context_record_indexing_configuration = ' .
        $this->getDatabaseConnection()->fullQuoteStr($indexingConfiguration, $this->tableName);

        $this->removeByWhereClause($whereClause);
    }

    /**
     * @param string $context
     * @param Site $site
     * @param string $tableName
     * @param integer uid
     * @param integer $fileUid If set, only given file will be removed
     *
     * @return void
     */
    public function removeByTableAndUidInContext($context, Site $site, $tableName, $uid, $fileUid = false)
    {
        $whereClause = 'context_site = ' . (int)$site->getRootPageId() .
            ' AND context_type = ' . $this->getDatabaseConnection()->fullQuoteStr($context, $this->tableName) .
            ' AND context_record_table = ' . $this->getDatabaseConnection()->fullQuoteStr($tableName, $this->tableName) .
            ' AND context_record_uid = ' . (int)$uid;

        // limit to certain file?
        if ($fileUid !== false) {
            $whereClause .= ' AND file=' . (int) $fileUid;
        }

        $this->removeByWhereClause($whereClause);
    }

    /**
     * @param Site $site
     * @param string $tableName
     * @param int $uid
     * @param int $language
     * @param string $fieldName
     * @param int[] $relatedFiles
     *
     * @return void
     */
    public function removeOldEntriesFromFieldInRecordContext(Site $site, $tableName, $uid, $language, $fieldName, array $relatedFiles)
    {
        array_walk($relatedFiles, 'intval');
        $whereClause = 'context_site = ' . (int)$site->getRootPageId() . ' AND context_type = "record" ' .
            ' AND context_record_table = ' . $this->getDatabaseConnection()->fullQuoteStr($tableName, $this->tableName) .
            ' AND context_record_uid = ' . (int)$uid .
            ' AND context_language = ' . (int)$language .
            ' AND context_record_field = ' . $this->getDatabaseConnection()->fullQuoteStr($fieldName, $this->tableName) .
            ($relatedFiles !== [] ? ' AND file NOT IN (' . implode(',', $relatedFiles) . ')' : '');

        $this->removeByWhereClause($whereClause);
    }

    /**
     * @param Site $site
     * @param int $pageId
     * @param int $language
     * @param int[] $relatedFiles
     *
     * @return void
     */
    public function removeOldEntriesInPageContext(Site $site, $pageId, $language = -1, array $relatedFiles = [])
    {
        array_walk($relatedFiles, 'intval');
        $whereClause = 'context_site = ' . (int)$site->getRootPageId() . ' AND context_type = "page" ' .
            ' AND context_record_page = ' . (int)$pageId;

        if ($relatedFiles !== []) {
            $whereClause .= ' AND file NOT IN (' . implode(',', $relatedFiles) . ')';
        }

        if ($language !== -1) {
            $whereClause .= ' AND context_language = ' . (int)$language;
        }

        $this->removeByWhereClause($whereClause);
    }

    /**
     * Remove queue entries by given file storage uid, considering the site
     *
     * @param Site $site
     * @param int $fileStorageUid
     * @param string $indexingConfiguration
     *
     * @return void
     */
    public function removeByFileStorage(Site $site, $fileStorageUid, $indexingConfiguration = null)
    {
        $whereClause = 'context_site = ' . (int) $site->getRootPageId()
                . ' AND context_additional_fields LIKE \'%"fileStorage":' . (int)$fileStorageUid . '%\''
                . ' AND context_type = \'storage\'';

        if ($indexingConfiguration) {
            $whereClause .= ' AND context_record_indexing_configuration = ' . $this->getDatabaseConnection()->fullQuoteStr($indexingConfiguration, $this->tableName);
        }

        $this->removeByWhereClause($whereClause);
    }

    /**
     * @param RecordContext $context
     *
     * @return void
     */
    public function removeByPageContext(RecordContext $context)
    {
        // todo - start with " AND" - cannot work
        $whereClause = ' AND context_record_uid = ' . intval($context->getUid()) .
            ' AND context_record_table = ' . $this->getDatabaseConnection()->fullQuoteStr($context->getTable(), $this->tableName);

        $this->removeByWhereClause($whereClause);
    }

    /**
     * @param string $whereClause
     *
     * @return void
     */
    protected function removeByWhereClause($whereClause)
    {
        $uidsToDelete = array_keys($this->getDatabaseConnection()->exec_SELECTgetRows(
            'uid',
            $this->tableName,
            $whereClause, '', '', '', 'uid')
        );

        if ($uidsToDelete !== []) {
            $this->emitBeforeMultipleItemsRemovedFromQueue($uidsToDelete);
            $this->getDatabaseConnection()->exec_DELETEquery($this->tableName, 'uid IN (' . implode(',', $uidsToDelete) . ')');
            $this->emitMultipleItemsRemovedFromQueue($uidsToDelete);
            foreach ($uidsToDelete as $deletedUid) {
                if (array_key_exists($deletedUid, self::$identityMap)) {
                    unset(self::$identityMap[$deletedUid]);
                }
            }
        }
    }

    /*++++++++++++++++++++++++++++++*
     *                              *
     *       Objects Factory        *
     *                              *
     *++++++++++++++++++++++++++++++*/

    /**
     * @param array $rows
     *
     * @return Item[]
     */
    protected function createObjectsFromRowArray(array $rows)
    {
        $itemArray = [];
        foreach ($rows as $singleRow) {
            $itemArray[] = $this->createObject($singleRow);
        }

        return $itemArray;
    }

    /**
     * @param array $row
     *
     * @return Item
     */
    protected function createObject(array $row)
    {
        $uid = intval($row['uid']);
        if (!array_key_exists($uid, self::$identityMap)) {
            $context = $this->getContextFactory()->getByRecord($row);
            $file = $this->getResourceFactory()->getFileObject(intval($row['file']));
            $item = GeneralUtility::makeInstance(
                Item::class,
                $file, $context, $row['uid'], $row['error'] == 1, $row['last_update'], $row['last_indexed'],
                $row['merge_id']
            );
            self::$identityMap[$uid] = $item;
        } else {
            $item = self::$identityMap[$uid];
            $item->setError($row['error'] == 1);
            $item->setLastIndexed($row['last_indexed']);
            $item->setLastUpdate($row['last_update']);
        }

        return self::$identityMap[$uid];
    }

    /**
     * @return \ApacheSolrForTypo3\Solrfal\Context\ContextFactory
     */
    protected function getContextFactory()
    {
        return GeneralUtility::makeInstance(ContextFactory::class);
    }

    /**
     * @return \TYPO3\CMS\Core\Resource\ResourceFactory
     */
    protected function getResourceFactory()
    {
        return GeneralUtility::makeInstance(ResourceFactory::class);
    }

    /**
     * @return \TYPO3\CMS\Core\Database\DatabaseConnection
     */
    protected function getDatabaseConnection()
    {
        return $GLOBALS['TYPO3_DB'];
    }


    /*++++++++++++++++++++++++++++++*
     *                              *
     *           Signals            *
     *                              *
     *++++++++++++++++++++++++++++++*/

    /**
     * @param Item $item
     *
     * @return void
     */
    protected function emitItemRemovedFromQueue(Item $item)
    {
        $signalSlotDispatcher = GeneralUtility::makeInstance(Dispatcher::class);
        $signalSlotDispatcher->dispatch(__CLASS__, 'itemRemoved', [$item]);
    }

    /**
     * @param int[] $itemUids
     *
     * @return void
     */
    protected function emitMultipleItemsRemovedFromQueue(array $itemUids)
    {
        $signalSlotDispatcher = GeneralUtility::makeInstance(Dispatcher::class);
        $signalSlotDispatcher->dispatch(__CLASS__, 'multipleItemsRemoved', [$itemUids]);
    }


    /**
     * @param Item $item
     *
     * @return void
     */
    protected function emitBeforeItemRemovedFromQueue(Item $item)
    {
        $signalSlotDispatcher = GeneralUtility::makeInstance(Dispatcher::class);
        $signalSlotDispatcher->dispatch(__CLASS__, 'beforeItemRemoved', [$item]);
    }

    /**
     * @param int[] $itemUids
     *
     * @return void
     */
    protected function emitBeforeMultipleItemsRemovedFromQueue(array $itemUids)
    {
        $signalSlotDispatcher = GeneralUtility::makeInstance(Dispatcher::class);
        $signalSlotDispatcher->dispatch(__CLASS__, 'beforeMultipleItemsRemoved', [$itemUids]);
    }

    /*++++++++++++++++++++++++++++++*
     *                              *
     *           Helpers            *
     *                              *
     *++++++++++++++++++++++++++++++*/

    /**
     * @param $data
     *
     * @return string
     */
    protected function getWhereClauseForItemData($data)
    {
        if ((int)$data['uid'] > 0) {
            $whereClause = 'uid = ' . (int)$data['uid'];
        } else {
            // remove entries not relevant for existing check
            unset($data['context_access_restrictions']);
            unset($data['last_update']);
            unset($data['last_indexed']);
            unset($data['error']);
            unset($data['error_message']);
            $whereClauseParts = [];
            foreach ($data as $field => $value) {
                $whereClauseParts[] = $field . ' = ' . $this->getDatabaseConnection()->fullQuoteStr($value, $this->tableName);
            }
            $whereClause = implode(' AND ', $whereClauseParts);
        }

        return $whereClause;
    }

    /**
     * @param string $whereClause
     * @param string $limit
     * @return array
     */
    protected function fetchRecordsFromDatabase($whereClause = '', $limit = '')
    {
        return $this->getDatabaseConnection()->exec_SELECTgetRows(
            implode(',', $this->fields),
            $this->tableName,
            $whereClause,
            '',
            '',
            $limit
        ) ?: [];
    }
}
