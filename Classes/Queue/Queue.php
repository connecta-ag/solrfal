<?php
namespace ApacheSolrForTypo3\Solrfal\Queue;

/***************************************************************
 * Copyright notice
 *
 * (c) 2013 Steffen Ritter <steffen.ritter@typo3.org>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use ApacheSolrForTypo3\Solr\Domain\Index\Queue\Statistic\QueueStatistic;
use ApacheSolrForTypo3\Solr\IndexQueue\Queue as SolrIndexQueue;
use ApacheSolrForTypo3\Solr\Site;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class Queue extends SolrIndexQueue
{
    /**
     * @return \ApacheSolrForTypo3\Solrfal\Queue\ItemRepository
     */
    protected function getItemRepository()
    {
        return GeneralUtility::makeInstance(ItemRepository::class);
    }

    /**
     * Gets number of Index Queue items for a specific site / indexing configuration
     * optional parameter to limit the deleted items by indexing configuration.
     *
     * @param Site $site The site to search for.
     * @param string $indexingConfigurationName name of a specific indexing
     *      configuration
     * @deprecated since 4.1 will be removed in 4.2 use getStatisticsBySite()->getTotalCount() please
     * @return mixed Number of items (integer) or FALSE if something went
     *      wrong (boolean)
     */
    public function getItemsCountBySite(
        Site $site,
        $indexingConfigurationName = ''
    ) {
        GeneralUtility::logDeprecatedFunction();
        $solrConfiguration = $site->getSolrConfiguration();
        $table = $solrConfiguration->getIndexQueueTableNameOrFallbackToConfigurationName($indexingConfigurationName);
        if ($table !== 'sys_file_storage') {
            return parent::getItemsCountBySite($site, $indexingConfigurationName);
        }

        return $this->getItemRepository()->countBySiteAndIndexConfigurationName($site, $indexingConfigurationName);
    }

    /**
     * Extracts the number of pending, indexed and erroneous items from the
     * Index Queue.
     *
     * @param Site $site
     * @param string $indexingConfigurationName
     *
     * @return QueueStatistic
     */
    public function getStatisticsBySite(Site $site, $indexingConfigurationName = '')
    {
        $solrConfiguration = $site->getSolrConfiguration();
        $table = $solrConfiguration->getIndexQueueTableNameOrFallbackToConfigurationName($indexingConfigurationName);

        if ($table !== 'sys_file_storage') {
            return parent::getStatisticsBySite($site, $indexingConfigurationName);
        }

        return $this->getItemRepository()->getStatisticsBySite($site, $indexingConfigurationName);
    }
}
