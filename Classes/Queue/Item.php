<?php
namespace ApacheSolrForTypo3\Solrfal\Queue;

/***************************************************************
 * Copyright notice
 *
 * (c) 2013 Steffen Ritter <steffen.ritter@typo3.org>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use ApacheSolrForTypo3\Solrfal\Context\ContextInterface;
use TYPO3\CMS\Core\Resource\File;

/**
 * Class Item
 */
class Item
{

    /**
     * @var integer
     */
    protected $uid;

    /**
     * @var ContextInterface
     */
    protected $context;

    /**
     * @var integer
     */
    protected $lastUpdate;

    /**
     * @var integer
     */
    protected $lastIndexed;

    /**
     * @var File
     */
    protected $file;

    /**
     * @var boolean
     */
    protected $error = false;

    /**
     * @var string
     */
    protected $merge_id = '';

    /**
     * @return \ApacheSolrForTypo3\Solrfal\Context\ContextInterface
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * @param File $file
     * @param ContextInterface $context
     * @param integer $uid
     * @param boolean $error
     * @param integer $lastUpdate
     * @param integer $lastIndexed
     * @param string $mergeId
     */
    public function __construct(File $file, ContextInterface $context, $uid = 0, $error = false, $lastUpdate = 0, $lastIndexed = 0, $mergeId = '')
    {
        $this->uid = intval($uid);
        $this->file = $file;
        $this->context = $context;
        $this->lastIndexed = intval($lastIndexed);
        $this->lastUpdate = intval($lastUpdate);
        $this->error = (boolean)$error;
        $this->merge_id = $mergeId;
    }

    /**
     * @return boolean
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @return \TYPO3\CMS\Core\Resource\File
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @return int
     */
    public function getLastIndexed()
    {
        return $this->lastIndexed;
    }

    /**
     * @return int
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * @return int
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param boolean $error
     * @return void
     */
    public function setError($error)
    {
        $this->error = (boolean)$error;
    }

    /**
     * @param integer $lastIndexed
     * @return void
     */
    public function setLastIndexed($lastIndexed)
    {
        $this->lastIndexed = (int)$lastIndexed;
    }

    /**
     * @param integer $lastUpdate
     * @return void
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->lastUpdate = (int)$lastUpdate;
    }

    /**
     * @return string
     */
    public function getMergeId()
    {
        return $this->merge_id;
    }

    /**
     * @param string $merge_id
     */
    public function setMergeId($merge_id)
    {
        $this->merge_id = $merge_id;
    }
}
