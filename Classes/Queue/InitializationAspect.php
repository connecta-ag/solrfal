<?php
namespace ApacheSolrForTypo3\Solrfal\Queue;

/***************************************************************
 * Copyright notice
 *
 * (c) 2013 Steffen Ritter <steffen.ritter@typo3.org>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use ApacheSolrForTypo3\Solr\IndexQueue\InitializationPostProcessor;
use ApacheSolrForTypo3\Solr\Site;
use ApacheSolrForTypo3\Solrfal\Context\ContextFactory;
use TYPO3\CMS\Core\Log\LogManager;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class InitializationAspect
 */
class InitializationAspect implements InitializationPostProcessor
{
    /**
     * Post process Index Queue initialization
     *
     * @param Site $site The site to initialize
     * @param array $indexingConfigurations Initialized indexing configurations
     * @param array $initializationStatus Results of Index Queue initializations
     */
    public function postProcessIndexQueueInitialization(Site $site, array $indexingConfigurations, array $initializationStatus)
    {
        $detectors = $this->getContextDetectorsForSite($site);
        $this->getLogger()->info('Queue initialization triggered for site ' . $site->getSiteHash());
        foreach ($detectors as $contextDetector) {
            $contextDetector->initializeQueue($initializationStatus);
        }
    }

    /**
     * @param Site $site
     * @return \ApacheSolrForTypo3\Solrfal\Detection\RecordDetectionInterface[]
     */
    protected function getContextDetectorsForSite(Site $site)
    {
        return ContextFactory::getContextDetectors($site);
    }

    /**
     * @return \TYPO3\CMS\Core\Log\Logger
     */
    protected function getLogger()
    {
        return GeneralUtility::makeInstance(LogManager::class)->getLogger(__CLASS__);
    }
}
