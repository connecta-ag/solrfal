<?php
namespace ApacheSolrForTypo3\Solrfal\Scheduler;

/***************************************************************
 * Copyright notice
 *
 * (c) 2013 Steffen Ritter <steffen.ritter@typo3.org>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use ApacheSolrForTypo3\Solr\System\Environment\CliEnvironment;
use ApacheSolrForTypo3\Solrfal\Queue\ItemRepository;
use ApacheSolrForTypo3\Solrfal\Indexing\Indexer;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Scheduler\ProgressProviderInterface;
use TYPO3\CMS\Scheduler\Task\AbstractTask;

/**
 * Class IndexingTask
 */
class IndexingTask extends AbstractTask implements ProgressProviderInterface
{

    /**
     * @var integer
     */
    protected $fileCountLimit = 10;


    /**
     * @var string
     */
    protected $forcedWebRoot = '';

    /**
     * This is the main method that is called when a task is executed
     * It MUST be implemented by all classes inheriting from this one
     * Note that there is no error handling, errors and failures are expected
     * to be handled and logged by the client implementations.
     * Should return TRUE on successful execution, FALSE on error.
     *
     * @return bool Returns TRUE on successful execution, FALSE on error
     */
    public function execute()
    {
        $cliEnvironment = null;

        // Wrapped the CliEnvironment to avoid defining TYPO3_PATH_WEB since this
        // should only be done in the case when running it from outside TYPO3 BE
        // @see #921 and #934 on https://github.com/TYPO3-Solr
        if (TYPO3_REQUESTTYPE & TYPO3_REQUESTTYPE_CLI) {
            /** @var $cliEnvironment CliEnvironment */
            $cliEnvironment = GeneralUtility::makeInstance(CliEnvironment::class);
            $cliEnvironment->backup();
            $cliEnvironment->initialize($this->getWebRoot());
        }

        $this->getIndexer()->processIndexQueue($this->fileCountLimit, false);

        if (TYPO3_REQUESTTYPE & TYPO3_REQUESTTYPE_CLI) {
            $cliEnvironment->restore();
        }

        return true;
    }


    /**
     * Gets the progress of a task.
     *
     * @return float Progress of the task as a two decimal float. f.e. 44.87
     */
    public function getProgress()
    {
        $itemsIndexedPercentage = 0.0;

        $totalItemsCount = $this->getItemRepository()->count();
        $outstandingItemCount = $this->getItemRepository()->countIndexingOutstanding();
        $failureItemCount = $this->getItemRepository()->countFailures();

        if ($totalItemsCount > 0) {
            $itemsIndexedCount      = $totalItemsCount - $outstandingItemCount - $failureItemCount;
            $itemsIndexedPercentage = $itemsIndexedCount * 100 / $totalItemsCount;
            $itemsIndexedPercentage = round($itemsIndexedPercentage, 2);
        }

        return $itemsIndexedPercentage;
    }

    /**
     * Returns some additional information about indexing progress, shown in
     * the scheduler's task overview list.
     *
     * @return	string	Information to display
     */
    public function getAdditionalInformation()
    {
        $message = sprintf(
            $this->getLanguageService()->sL('LLL:EXT:solrfal/Resources/Private/Language/locallang.xlf:scheduler.additionalInformation'),
            $this->getItemRepository()->countFailures()
        );

        $message .=  ' / Using webroot: ' . htmlspecialchars($this->getWebRoot());

        return $message;
    }


    /**
     * In the cli context TYPO3 has chance to determine the webroot.
     * Since we need it for the TSFE related things we allow to set it
     * in the scheduler task and use the ###PATH_typo3### marker in the
     * setting to be able to define relative pathes.
     *
     * @return string
     */
    public function getWebRoot()
    {
        if ($this->forcedWebRoot !== '') {
            return $this->replaceWebRootMarkers($this->forcedWebRoot);
        }

        // when nothing is configured, we use the constant PATH_site
        // which should fit in the most cases
        return PATH_site;
    }

    /**
     * @param string $webRoot
     * @return string
     */
    protected function replaceWebRootMarkers($webRoot)
    {
        if (strpos($webRoot, '###PATH_typo3###') !== false) {
            $webRoot = str_replace('###PATH_typo3###', PATH_typo3, $webRoot);
        }

        if (strpos($webRoot, '###PATH_site###') !== false) {
            $webRoot = str_replace('###PATH_site###', PATH_site, $webRoot);
        }

        return $webRoot;
    }

    /**
     * @param integer $fileCountLimit
     * @return void
     */
    public function setFileCountLimit($fileCountLimit)
    {
        $this->fileCountLimit = intval($fileCountLimit);
    }

    /**
     * @return integer
     */
    public function getFileCountLimit()
    {
        return $this->fileCountLimit;
    }

    /**
     * @return \ApacheSolrForTypo3\Solrfal\Queue\ItemRepository
     */
    protected function getItemRepository()
    {
        return GeneralUtility::makeInstance(ItemRepository::class);
    }

    /**
     * @return \TYPO3\CMS\Lang\LanguageService
     */
    protected function getLanguageService()
    {
        return $GLOBALS['LANG'];
    }

    /**
     * @return \ApacheSolrForTypo3\Solrfal\Indexing\Indexer
     */
    protected function getIndexer()
    {
        return GeneralUtility::makeInstance(Indexer::class);
    }

    /**
     * @param string $forcedWebRoot
     */
    public function setForcedWebRoot($forcedWebRoot)
    {
        $this->forcedWebRoot = $forcedWebRoot;
    }

    /**
     * @return string
     */
    public function getForcedWebRoot()
    {
        return $this->forcedWebRoot;
    }
}
