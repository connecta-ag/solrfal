<?php
namespace ApacheSolrForTypo3\Solrfal\Domain\Model;

/***************************************************************
 * Copyright notice
 *
 * (c) 2016 Markus Friedrich <markus.friedrich@dkd.de>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Backend\Utility\BackendUtility;

/**
 * A reference index entry
 */
class ReferenceIndexEntry
{

    /**
     * Properties
     *
     * @var array $properties
     */
    protected $properties;

    /**
     * Table name
     *
     * @var string $tableName
     */
    protected $tableName;

    /**
     * Table field
     *
     * @var string $tableField
     */
    protected $tableField;

    /**
     * Record uid
     *
     * @var integer $recordUid
     */
    protected $recordUid;

    /**
     * Reference table name
     *
     * @var string $referenceTableName
     */
    protected $referenceTableName;

    /**
     * Reference uid
     *
     * @var integer $referenceUid
     */
    protected $referenceUid;

    /**
     * Class constructor
     *
     * @param array $referenceIndexRow
     * @return void
     */
    public function __construct(array $referenceIndexRow)
    {
        $this->properties = $referenceIndexRow;
        $this->tableName = $referenceIndexRow['tablename'];
        $this->tableField = $referenceIndexRow['field'];
        $this->recordUid = (int) $referenceIndexRow['recuid'];
        $this->referenceTableName = $referenceIndexRow['ref_table'];
        $this->referenceUid = (int) $referenceIndexRow['ref_uid'];
    }

    /**
     * Returns the table name
     *
     * @return string
     */
    public function getTableName()
    {
        return $this->tableName;
    }

    /**
     * Returns the table field
     *
     * @return string
     */
    public function getTableField()
    {
        return $this->tableField;
    }

    /**
     * Returns the record uid
     *
     * @return integer
     */
    public function getRecordUid()
    {
        return $this->recordUid;
    }

    /**
     * Returns the record
     *
     * @return array
     */
    public function getRecord()
    {
        return BackendUtility::getRecord($this->getTableName(), $this->getRecordUid());
    }

    /**
     * Returns the reference table name
     *
     * @return string
     */
    public function getReferenceTableName()
    {
        return $this->referenceTableName;
    }

    /**
     * Returns the tablename
     *
     * @return integer
     */
    public function getReferenceUid()
    {
        return $this->referenceUid;
    }

    /**
     * Returns a record hash identifing the referenced record
     *
     * @return string
     */
    public function getRecordHash()
    {
        return md5($this->getTableName() . $this->getRecordUid());
    }
}
