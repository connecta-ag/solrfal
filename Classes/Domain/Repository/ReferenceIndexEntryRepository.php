<?php
namespace ApacheSolrForTypo3\Solrfal\Domain\Repository;

/***************************************************************
 * Copyright notice
 *
 * (c) 2016 Markus Friedrich <markus.friedrich@dkd.de>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use ApacheSolrForTypo3\Solrfal\Domain\Model\ReferenceIndexEntry;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Reference index entry repository
 */
class ReferenceIndexEntryRepository
{

    /**
     * Database connection
     *
     * @var \TYPO3\CMS\Core\Database\DatabaseConnection
     */
    protected $databaseConnection;


    /**
     * Class constructor
     *
     * @return void
     */
    public function __construct()
    {
        $this->databaseConnection = $GLOBALS['TYPO3_DB'];
    }

    /**
     * Returns reference index entry
     *
     * @param array $referenceData
     *
     * @return \ApacheSolrForTypo3\Solrfal\Domain\Model\ReferenceIndexEntry
     */
    protected function getReferenceIndexEntry(array $referenceData)
    {
        return GeneralUtility::makeInstance(ReferenceIndexEntry::class, $referenceData);
    }

    /**
     * Find by reference record
     *
     * @param string $referenceTable
     * @param string $referenceUid
     * @param array $excludeTables
     * @param array $limitToTables
     *
     * @return \ApacheSolrForTypo3\Solrfal\Domain\Model\ReferenceIndexEntry[]
     */
    public function findByReferenceRecord($referenceTable, $referenceUid, array $excludeTables = [], array $limitToTables = [])
    {

        // build table constraint
        $tablesConstraint = '';
        if ($excludeTables) {
            $tablesConstraint .= ' AND tablename NOT IN (' . implode(',', $this->databaseConnection->fullQuoteArray($excludeTables, 'sys_refindex')) .')';
        }
        if ($limitToTables) {
            $tablesConstraint .= ' AND tablename IN (' . implode(',', $this->databaseConnection->fullQuoteArray($limitToTables, 'sys_refindex')) .')';
        }

        $referencesData = $this->databaseConnection->exec_SELECTgetRows(
            '*',
            'sys_refindex',
            'ref_table=' . $this->databaseConnection->fullQuoteStr($referenceTable, 'sys_refindex')
                . ' AND ref_uid=' . (int) $referenceUid
                . ' AND deleted=0'
                . $tablesConstraint
        );
        $referenceIndexEntries = [];
        foreach ($referencesData as $referenceData) {
            $referenceIndexEntries[] = $this->getReferenceIndexEntry($referenceData);
        }

        return $referenceIndexEntries;
    }

    /**
     * Find reference by reference index entry
     *
     * @param \ApacheSolrForTypo3\Solrfal\Domain\Model\ReferenceIndexEntry $referenceIndexEntry
     * @param array $excludeTables
     *
     * @return \ApacheSolrForTypo3\Solrfal\Domain\Model\ReferenceIndexEntry
     */
    public function findOneByReferenceIndexEntry(ReferenceIndexEntry $referenceIndexEntry, array $excludeTables = [])
    {

        // build exclude table constraint
        if ($excludeTables) {
            $excludeTablesConstraint = ' AND tablename NOT IN (' . implode(',', $this->databaseConnection->fullQuoteArray($excludeTables, 'sys_refindx')) .')';
        } else {
            $excludeTablesConstraint = '';
        }

        $referenceData = $this->databaseConnection->exec_SELECTgetSingleRow(
            '*',
            'sys_refindex',
            'ref_table=' . $this->databaseConnection->fullQuoteStr($referenceIndexEntry->getTableName(), 'sys_refindex')
                . ' AND ref_uid=' . $referenceIndexEntry->getRecordUid()
                . ' AND deleted=0'
                . $excludeTablesConstraint
        );
        if ($referenceData) {
            $referenceIndexEntry = $this->getReferenceIndexEntry($referenceData);
        } else {
            $referenceIndexEntry = null;
        }

        return $referenceIndexEntry;
    }
}
