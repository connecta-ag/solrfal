<?php
namespace ApacheSolrForTypo3\Solrfal\System\Language;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017- Timo Hund <timo.hund@dkd.de
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\Page\PageRepository;

/**
 * This class encapsulates the access to the extension configuration.
 *
 * @package ApacheSolrForTypo3\Solrfal\System\Configuration
 * @author Timo Hund <timo.hund@dkd.de>
 */
class OverlayService
{

    /**
     * @var PageRepository
     */
    protected $pageRepository;

    /**
     * @return PageRepository
     */
    protected function getInitializedPageRepository()
    {
        if ($this->pageRepository !== null) {
            return $this->pageRepository;
        }

        /** \TYPO3\CMS\Frontend\Page\PageRepository $page */
        $this->pageRepository = GeneralUtility::makeInstance(PageRepository::class);
        $this->pageRepository->init(false);
        return $this->pageRepository;
    }

    /**
     * Returns an overlayed version of a record.
     *
     * @param string $tableName
     * @param array $record
     * @param integer $languageUid
     * @param string $overlayMode
     * @return mixed
     */
    public function getRecordOverlay($tableName, $record, $languageUid, $overlayMode = '')
    {
        /**
         * @todo This is required for the frontend restriction container,
         * when used in the backend for TYPO3 9 the overlaying should be done with an own method
         */
        if (!isset($GLOBALS['TSFE'])) {
            $GLOBALS['TSFE'] = new \stdClass();
        }
        if (!isset($GLOBALS['TSFE']->gr_list)) {
            $GLOBALS['TSFE']->gr_list = '';
        }

        return $this->getInitializedPageRepository()->getRecordOverlay($tableName, $record, (int)$languageUid, $overlayMode);
    }
}