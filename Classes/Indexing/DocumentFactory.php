<?php
namespace ApacheSolrForTypo3\Solrfal\Indexing;

/***************************************************************
 * Copyright notice
 *
 * (c) 2013 Steffen Ritter <steffen.ritter@typo3.org>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use ApacheSolrForTypo3\Solr\Domain\Variants\IdBuilder;
use ApacheSolrForTypo3\Solr\HtmlContentExtractor;
use ApacheSolrForTypo3\Solr\Util;
use ApacheSolrForTypo3\Solrfal\Context\RecordContext;
use ApacheSolrForTypo3\Solrfal\Queue\Item;
use ApacheSolrForTypo3\Solrfal\Queue\ItemGroup;
use ApacheSolrForTypo3\Solrfal\Service\FieldProcessingService;
use ApacheSolrForTypo3\Solrfal\System\Language\OverlayService;
use ApacheSolrForTypo3\Tika\Service\Extractor\TextExtractor;
use TYPO3\CMS\Core\Log\LogManager;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Resource\TextExtraction\TextExtractorRegistry;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Extbase\SignalSlot\Dispatcher;
use TYPO3\CMS\Frontend\Page\PageRepository;

/**
 * Class DocumentFactory
 */
class DocumentFactory
{

    const SOLR_TYPE = 'tx_solr_file';

    /**
     * @var IdBuilder
     */
    protected $variantIdBuilder = null;

    /**
     * DocumentFactory constructor.
     * @param IdBuilder|null $variantIdBuilder
     */
    public function __construct(IdBuilder $variantIdBuilder = null)
    {
        $this->variantIdBuilder = isset($variantIdBuilder) ? $variantIdBuilder : GeneralUtility::makeInstance(IdBuilder::class);
    }

    /**
     * @param Item $item
     * @return \Apache_Solr_Document
     */
    public function createDocumentForQueueItem(Item $item)
    {
        $document = GeneralUtility::makeInstance('Apache_Solr_Document');

        $this->addFileInformation($document, $item);

        try {
            $this->addFileTextContent($document, $item);
        } catch (NoSuiteableExtractorFoundException $e) {
            // no extractor found to extract text, we might want to index
                // the document without the text content but log an info message
            $this->getLogger()->info($e->getMessage());
        }
        $this->addContextInformation($document, $item);
        $this->addFieldsFromTypoScriptConfiguration($document, $item);

        FieldProcessingService::processFieldInstructions($document, $item->getContext());

        // only add endtime to solr document if it was set in referencing document
        // otherwise the Solr garbage collector will remove it (since Solr Server 4.8)
        if (isset($document->endtime) && (int)$document->endtime === 0) {
            unset($document->endtime);
        }

        return $document;
    }

    /**
     * @param ItemGroup $itemGroup
     * @param boolean $merge
     * @return array[\Apache_Solr_Document]
     */
    public function createDocumentsForQueueItemGroup(ItemGroup $itemGroup, $merge = false)
    {
        $documents = [];
        if ($merge) {
            // merging is active we merge all items into one solr document
            $rootItemDocument = $this->createDocumentForQueueItem($itemGroup->getRootItem());
            $configuration = $itemGroup->getRootItem()->getContext()->getSite()->getSolrConfiguration();
            $mapping = $configuration->getObjectByPathOrDefault('plugin.tx_solr.index.enableFileIndexing.mergeDuplicates.fieldMapping.', []);
            $mapping = $this->addDefaultMergeMapping($mapping);

            foreach ($itemGroup->getItems() as $mergedItem) {
                // no merge items
                $mergeItemDocument = $this->createDocumentForQueueItem($mergedItem);
                foreach ($mapping as $sourceField => $targetField) {
                    $sourceField = $mergeItemDocument->getField($sourceField);
                    $sourceFieldValue = isset($sourceField['value']) ? $sourceField['value'] : null;

                    if ($sourceFieldValue === null) {
                        // nothing to add
                        continue;
                    }

                    $targetFieldArray = $rootItemDocument->getField($targetField);
                    $targetFieldValue = $this->getFieldValueAsArray($targetFieldArray);
                    $valueAllReadyPresent = in_array($sourceFieldValue, $targetFieldValue);

                    if (!$valueAllReadyPresent) {
                        $rootItemDocument->addField($targetField, $sourceFieldValue);
                    }
                }
            }

            $documents[] = $rootItemDocument;
        } else {
            // no merging is active, we create one item per group
            foreach ($itemGroup->getItems() as $item) {
                $documents[] = $this->createDocumentForQueueItem($item);
            }
        }

        return $documents;
    }

    /**
     * Makes sure to retrieve the field value as an array.
     *
     * @param array $targetFieldArray
     * @return array
     */
    protected function getFieldValueAsArray($targetFieldArray)
    {
        if (empty($targetFieldArray['value'])) {
            $targetFieldValue = [];
            return $targetFieldValue;
        } elseif (is_array($targetFieldArray['value'])) {
            $targetFieldValue = $targetFieldArray['value'];
            return $targetFieldValue;
        } else {
            $targetFieldValue = [$targetFieldArray['value']];
            return $targetFieldValue;
        }
    }

    /**
     * @param array $mapping
     * @return mixed
     */
    protected function addDefaultMergeMapping(array $mapping)
    {
        // we allways want to merge the access field
        $mapping['access'] = 'access';

        return $mapping;
    }

    /**
     * @return \TYPO3\CMS\Core\Log\Logger
     */
    protected function getLogger()
    {
        return GeneralUtility::makeInstance(LogManager::class)->getLogger(__CLASS__);
    }

    /**
     * @param \Apache_Solr_Document $document
     * @param Item $item
     * @return void
     */
    protected function addFileInformation(\Apache_Solr_Document $document, Item $item)
    {
        $file = $item->getFile();

        // file meta data, reference
        $document->setField('title',         $file->getName());
        $document->setField('created',       $file->getCreationTime());
        $document->setField('changed',       $file->getModificationTime());

        $document->setField('fileStorage',   $file->getStorage()->getUid());
        $document->setField('fileUid',       $file->getUid());
        $document->setField('fileMimeType',  $file->getMimeType());
        $document->setField('fileName',      $file->getName());
        $document->setField('fileSize',      $file->getSize());
        $document->setField('fileExtension', $file->getExtension());
        $document->setField('fileSha1',      $file->getSha1());

        $document->setField('filePublicUrl', $file->getPublicUrl());
        $document->setField('url',           $file->getPublicUrl());

        $this->emitAddedSolrFileInformation($file);
    }

    /**
     * @param \Apache_Solr_Document $document
     * @param Item $item
     * @throws NoSuiteableExtractorFoundException
     * @return void
     */
    protected function addFileTextContent(\Apache_Solr_Document $document, Item $item)
    {
        $file = $item->getFile();

        if (version_compare(TYPO3_branch, '7.2', '>=')) {
            $extractorRegistry = TextExtractorRegistry::getInstance();
            $extractor = $extractorRegistry->getTextExtractor($file);
        } elseif (ExtensionManagementUtility::isLoaded('tika')) {
            $extractor = GeneralUtility::makeInstance(TextExtractor::class);
        }

        if (!is_object($extractor) || !method_exists($extractor, 'canExtractText')) {
            $message = 'No extractor for text extraction found for file '.$file->getName(). '. Extractor registered or tika installed and solrfal file pattern configured correctly?';
            throw new NoSuiteableExtractorFoundException($message);
        }

        if ($extractor->canExtractText($file)) {
            $fileTextContent = $extractor->extractText($file);
            $fileTextContent = HtmlContentExtractor::cleanContent($fileTextContent);

            $document->setField('content', $fileTextContent);
        }
    }

    /**
     * @param \Apache_Solr_Document $document
     * @param Item $item
     * @return void
     */
    protected function addContextInformation(\Apache_Solr_Document $document, Item $item)
    {
        $context = $item->getContext();

        // Add additional context fields first, so they can't override necessary fields
        foreach ($context->getAdditionalStaticDocumentFields() as $key => $value) {
            $document->setField($key, $value);
        }

        foreach ($context->getAdditionalDynamicDocumentFields($item->getFile()) as $key => $value) {
            $document->setField($key, $value);
        }

        // system fields
        $document->setField('uid', $item->getUid());
        $document->setField('pid', $context->getPageId());

        if ($document->getField('access') === false) {
            $accessRights = $context->getAccessRestrictions()->__toString();
            $document->setField('access', empty($accessRights) ? 'c:0' : $accessRights);
        }

        $document->setField('id',       $this->calculateDocumentId($item));
        $document->setField('site',     $context->getSite()->getDomain());
        $document->setField('siteHash', $context->getSite()->getSiteHash());
        $document->setField('appKey',   'EXT:solrfal');
        $document->setField('type',     self::SOLR_TYPE);

        $variantId = $this->variantIdBuilder->buildFromTypeAndUid(self::SOLR_TYPE, $item->getFile()->getUid());
        $document->setField('variantId', $variantId);
    }

    /**
     * @param \Apache_Solr_Document $document
     * @param Item $item
     * @return void
     */
    protected function addFieldsFromTypoScriptConfiguration(\Apache_Solr_Document $document, Item $item)
    {
        $translatedMetaData = $this->getTranslatedFileMetaData($item->getFile(), $item->getContext()->getLanguage());
        $this->emitFileMetaDataRetrieved($item, $translatedMetaData);

        $fileConfiguration = Util::getSolrConfigurationFromPageId(
            $item->getContext()->getSite()->getRootPageId(),
            false,
            $item->getContext()->getLanguage()
        );

        $fieldConfiguration = $fileConfiguration->getObjectByPathOrDefault('plugin.tx_solr.index.queue._FILES.default.', []);
        $contextConfiguration = $item->getContext()->getSpecificFieldConfigurationTypoScript();

        if (count($contextConfiguration) > 0) {
            ArrayUtility::mergeRecursiveWithOverrule(
                $fieldConfiguration,
                $contextConfiguration
            );
        }

        $fieldConfigurationInRecordContext = null;
        if (is_array($fieldConfiguration['__RecordContext.'])) {
            $fieldConfigurationInRecordContext = $fieldConfiguration['__RecordContext.'];
            unset($fieldConfiguration['__RecordContext']);
            unset($fieldConfiguration['__RecordContext.']);
        }

        FieldProcessingService::addTypoScriptFieldsToDocument($item->getContext(), $document, $fieldConfiguration, $translatedMetaData);

        if ($item->getContext() instanceof RecordContext && is_array($fieldConfigurationInRecordContext)) {
            /** @var RecordContext $recordContext */
            $recordContext = $item->getContext();
            $relatedRecord = $this->getDatabaseConnection()->exec_SELECTgetSingleRow(
                '*',
                $recordContext->getTable(),
                'uid = ' . $recordContext->getUid()
            );
            $relatedRecord = $this->getTranslatedRecord($recordContext->getTable(), $relatedRecord, $recordContext->getLanguage());

            FieldProcessingService::addTypoScriptFieldsToDocument(
                $item->getContext(),
                $document,
                $fieldConfigurationInRecordContext,
                $relatedRecord
            );
        }
    }

    /**
     * @param File $file
     * @param int $language
     * @return array
     */
    protected function getTranslatedFileMetaData(File $file, $language = 0)
    {
        $metaData = $file->_getMetaData();
        if ($metaData['sys_language_uid'] !== $language) {
            $metaData = $this->getTranslatedRecord('sys_file_metadata', $metaData, $language);
        }
        return $metaData;
    }

    /**
     * Retrieves an translated record with valid overlays according to the TCA configuration
     *
     * @param string $table
     * @param array $record
     * @param int $requestedLanguage
     * @return array
     */
    protected function getTranslatedRecord($table, array $record, $requestedLanguage = 0)
    {
        $languageOfIncomingRecord = (int)$record[$GLOBALS['TCA'][$table]['ctrl']['languageField']];
        if (
            // default language requested and present
            !($requestedLanguage === 0 and $languageOfIncomingRecord <= 0) &&
            // but no parent field for overlays existing
            isset($GLOBALS['TCA'][$table]['ctrl']['transOrigPointerField'])
        ) {
            $originalRecord = false;
            if ($languageOfIncomingRecord === 0) {
                $originalRecord = $record;
            } else {
                $originalRecordUid = (int)$requestedLanguage[$GLOBALS['TCA'][$table]['ctrl']['transOrigPointerField']];
                if ($originalRecordUid > 0) {
                    $originalRecord = $this->getDatabaseConnection()->exec_SELECTgetSingleRow('*', $table, 'uid = ' . $originalRecordUid);
                }
            }
            if (is_array($originalRecord)) {
                if ((int)$originalRecord[$GLOBALS['TCA'][$table]['ctrl']['languageField']] !== $requestedLanguage) {
                    /** @var $overlayService OverlayService */
                    $overlayService = GeneralUtility::makeInstance(OverlayService::class);
                    $record = $overlayService->getRecordOverlay(
                        $table,
                        $record,
                        (int)$requestedLanguage
                    );
                }
            }
        }
        return $record;
    }

    /**
     * @param Item $item
     *
     * @return string
     */
    protected function calculateDocumentId(Item $item)
    {
        return Util::getDocumentId(
            self::SOLR_TYPE,
            $item->getContext()->getPageId(),
            $item->getFile()->getUid(),
            md5(implode(',', $item->getContext()->toArray()))
        );
    }

    /**
     * @param File $file
     * @return void
     */
    protected function emitAddedSolrFileInformation(File $file)
    {
        /** @var \TYPO3\CMS\Extbase\SignalSlot\Dispatcher $signalSlotDispatcher */
        $signalSlotDispatcher = GeneralUtility::makeInstance(Dispatcher::class);
        $signalSlotDispatcher->dispatch(__CLASS__, 'addedSolrFileInformation', [$file]);
    }

    /**
     * @param Item $item
     * @param array $metaData
     * @return void
     */
    protected function emitFileMetaDataRetrieved(Item $item, array &$metaData)
    {
        $arrayObject = new \ArrayObject($metaData);
        /** @var \TYPO3\CMS\Extbase\SignalSlot\Dispatcher $signalSlotDispatcher */
        $signalSlotDispatcher = GeneralUtility::makeInstance(Dispatcher::class);
        $signalSlotDispatcher->dispatch(__CLASS__, 'fileMetaDataRetrieved', [$item, &$arrayObject]);
        $metaData = $arrayObject->getArrayCopy();
    }

    /**
     * @return \TYPO3\CMS\Core\Database\DatabaseConnection
     */
    protected function getDatabaseConnection()
    {
        return $GLOBALS['TYPO3_DB'];
    }
}
