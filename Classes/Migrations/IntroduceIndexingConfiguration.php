<?php
namespace ApacheSolrForTypo3\Solrfal\Migrations;

/***************************************************************
 * Copyright notice
 *
 * (c) 2015 Steffen Ritter <steffen.ritter@typo3.org>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use ApacheSolrForTypo3\Solrfal\Queue\ItemRepository;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class IntroduceIndexingConfiguration
 */
class IntroduceIndexingConfiguration implements Migration
{

    /**
     * @return boolean
     */
    public function isNeeded()
    {
        $result = $this->getDatabase()->admin_query(
            'SHOW COLUMNS FROM tx_solr_indexqueue_file LIKE \'context_record_indexing_configuration\''
        );
        $count = (int)$this->getDatabase()->sql_num_rows($result);

        if ($count > 0) {
            return $this->getDatabase()->exec_SELECTcountRows(
                'uid', '
				tx_solr_indexqueue_file',
                'context_type = \'record\' AND context_record_indexing_configuration = \'\''
            ) > 0;
        }
        return true;
    }

    /**
     * @return array
     */
    public function process()
    {
        $currentTableFields = $this->getDatabase()->admin_get_fields('tx_solr_indexqueue_file');
        $message = '';
        $status = FlashMessage::OK;
        if (!$currentTableFields['context_record_indexing_configuration']) {
            $sql = 'ALTER TABLE tx_solr_indexqueue_file ' .
                        'ADD COLUMN context_record_indexing_configuration VARCHAR(255) DEFAULT \'\' NOT NULL';
            if ($this->getDatabase()->admin_query($sql) === false) {
                $message = ' SQL ERROR: ' . $this->getDatabase()->sql_error();
                return [FlashMessage::ERROR, 'Migration failed', $message];
            } else {
                $message = 'Added field for indexing_configuration!' . LF;
            }
        }

        if ($this->getDatabase()->exec_SELECTcountRows(
                'uid', '
				tx_solr_indexqueue_file',
                'context_type = \'record\' AND context_record_indexing_configuration = \'\''
            ) > 0) {
            /** @var \ApacheSolrForTypo3\Solrfal\Queue\ItemRepository $repository */
            $repository = GeneralUtility::makeInstance(ItemRepository::class);
            $repository->purgeContext('record');
            $message .= 'Purged all index items for record context. Please reinitialize queues.';
        }

        return [$status, 'Migration successful', $message];
    }

    /**
     * @return \TYPO3\CMS\Core\Database\DatabaseConnection
     */
    protected function getDatabase()
    {
        return $GLOBALS['TYPO3_DB'];
    }
}
