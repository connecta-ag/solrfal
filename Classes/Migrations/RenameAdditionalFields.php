<?php
namespace ApacheSolrForTypo3\Solrfal\Migrations;

/***************************************************************
 * Copyright notice
 *
 * (c) 2015 Steffen Ritter <steffen.ritter@typo3.org>
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 * A copy is found in the textfile GPL.txt and important notices to the license
 * from the author is found in LICENSE.txt distributed with these scripts.
 *
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Messaging\FlashMessage;

/**
 * Class RenameAdditionalFields
 */
class RenameAdditionalFields implements Migration
{

    /**
     * @var string
     */
    protected $tableName = 'tx_solr_indexqueue_file';

    /**
     * @var string
     */
    protected $oldFieldName = 'context_addditional_fields';

    /**
     * @var string
     */
    protected $newFieldName = 'context_additional_fields';

    /**
     * @return boolean
     */
    public function isNeeded()
    {
        $result = $GLOBALS['TYPO3_DB']->admin_query(
            'SHOW COLUMNS FROM ' . $this->tableName . ' LIKE \'' . $this->oldFieldName . '\''
        );
        $count = $GLOBALS['TYPO3_DB']->sql_num_rows($result);

        return $count > 0;
    }

    public function process()
    {
        $title = 'Renaming "' . $this->tableName . ':' . $this->oldFieldName .
            '" to "' . $this->tableName . ':' . $this->newFieldName . '": ';

        $currentTableFields = $GLOBALS['TYPO3_DB']->admin_get_fields($this->tableName);

        if ($currentTableFields[$this->newFieldName]) {
            $message = 'Field ' . $this->tableName . ':' . $this->newFieldName . ' already existing.';
            $status = FlashMessage::OK;
        } else {
            if (!$currentTableFields[$this->oldFieldName]) {
                $message = 'Field ' . $this->tableName . ':' . $this->oldFieldName . ' not existing';
                $status = FlashMessage::ERROR;
            } else {
                $sql = 'ALTER TABLE ' . $this->tableName . ' CHANGE COLUMN ' . $this->oldFieldName . ' ' . $this->newFieldName . ' ' .
                    $currentTableFields[$this->oldFieldName]['Type'];

                if ($GLOBALS['TYPO3_DB']->admin_query($sql) === false) {
                    $message = ' SQL ERROR: ' . $GLOBALS['TYPO3_DB']->sql_error();
                    $status = FlashMessage::ERROR;
                } else {
                    $message = 'OK!';
                    $status = FlashMessage::OK;
                }
            }
        }

        return [$status, $title, $message];
    }
}
