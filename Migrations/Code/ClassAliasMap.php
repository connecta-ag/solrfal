<?php

return [
	'TYPO3\Solr\Solrfal\Context\AbstractContext' => 'ApacheSolrForTypo3\Solrfal\Context\AbstractContext',
	'TYPO3\Solr\Solrfal\Context\ContextFactory' => 'ApacheSolrForTypo3\Solrfal\Context\ContextFactory',
	'TYPO3\Solr\Solrfal\Context\ContextFactoryInterface' => 'ApacheSolrForTypo3\Solrfal\Context\ContextFactoryInterface',
	'TYPO3\Solr\Solrfal\Context\ContextInterface' => 'ApacheSolrForTypo3\Solrfal\Context\ContextInterface',
	'TYPO3\Solr\Solrfal\Context\PageContext' => 'ApacheSolrForTypo3\Solrfal\Context\PageContext',
	'TYPO3\Solr\Solrfal\Context\RecordContext' => 'ApacheSolrForTypo3\Solrfal\Context\RecordContext',
	'TYPO3\Solr\Solrfal\Context\StorageContext' => 'ApacheSolrForTypo3\Solrfal\Context\StorageContext',

	'TYPO3\Solr\Solrfal\Detection\AbstractRecordDetector' => 'ApacheSolrForTypo3\Solrfal\Detection\AbstractRecordDetector',
	'TYPO3\Solr\Solrfal\Detection\PageContextDetector' => 'ApacheSolrForTypo3\Solrfal\Detection\PageContextDetector',
	'TYPO3\Solr\Solrfal\Detection\PageContextDetectorFrontendIndexingAspect' => 'ApacheSolrForTypo3\Solrfal\Detection\PageContextDetectorFrontendIndexingAspect',
	'TYPO3\Solr\Solrfal\Detection\RecordContextDetector' => 'ApacheSolrForTypo3\Solrfal\Detection\RecordContextDetector',
	'TYPO3\Solr\Solrfal\Detection\RecordDetectionInterface' => 'ApacheSolrForTypo3\Solrfal\Detection\RecordDetectionInterface',
	'TYPO3\Solr\Solrfal\Detection\StorageContextDetector' => 'ApacheSolrForTypo3\Solrfal\Detection\StorageContextDetector',

	'TYPO3\Solr\Solrfal\Indexing\DocumentFactory' => 'ApacheSolrForTypo3\Solrfal\Indexing\DocumentFactory',
	'TYPO3\Solr\Solrfal\Indexing\Indexer' => 'ApacheSolrForTypo3\Solrfal\Indexing\Indexer',

	'TYPO3\Solr\Solrfal\Queue\ConsistencyAspect' => 'ApacheSolrForTypo3\Solrfal\Queue\ConsistencyAspect',
	'TYPO3\Solr\Solrfal\Queue\InitializationAspect' => 'ApacheSolrForTypo3\Solrfal\Queue\InitializationAspect',
	'TYPO3\Solr\Solrfal\Queue\Item' => 'ApacheSolrForTypo3\Solrfal\Queue\Item',
	'TYPO3\Solr\Solrfal\Queue\ItemRepository' => 'ApacheSolrForTypo3\Solrfal\Queue\ItemRepository',

	'TYPO3\Solr\Solrfal\Scheduler\IndexingTask' => 'ApacheSolrForTypo3\Solrfal\Scheduler\IndexingTask',
	'TYPO3\Solr\Solrfal\Scheduler\IndexingTaskAdditionalFieldProvider' => 'ApacheSolrForTypo3\Solrfal\Scheduler\IndexingTaskAdditionalFieldProvider',

	'TYPO3\Solr\Solrfal\Service\FieldProcessingService' => 'ApacheSolrForTypo3\Solrfal\Service\FieldProcessingService',
	'TYPO3\Solr\Solrfal\Service\FileAttachmentResolver' => 'ApacheSolrForTypo3\Solrfal\Service\FileAttachmentResolver',
];

