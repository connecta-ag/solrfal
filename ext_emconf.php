<?php
$EM_CONF[$_EXTKEY] = [
	'title' => 'Apache Solr for TYPO3 - File Indexing',
	'description' => 'Add indexing for FileAbstractionLayer based files in TYPO3 CMS.',
	'category' => 'misc',
	'state' => 'stable',
	'uploadfolder' => '0',
	'createDirs' => '',
	'author' => 'Steffen Ritter, Timo Hund, Markus Friedrich, Rafael Kähm',
	'author_email' => 'solr-eb-support@dkd.de',
	'author_company' => '[rs]websystems',
	'clearCacheOnLoad' => 1,
	'version' => '4.2.1',
	'constraints' =>
	[
		'depends' => [
			'typo3' => '8.7.0-8.9.99',
			'solr' => '7.0.0-',
			'filemetadata' => '8.7.0-'
		],
		'conflicts' => [],
		'suggests' => [
			'tika' => '2.0.0-'
		],
	],
	'autoload' => [
		'psr-4' => [
			'ApacheSolrForTypo3\\Solrfal\\' => 'Classes'
		]
	],
	'autoload-dev' => [
		'psr-4' => [
			'ApacheSolrForTypo3\\Solrfal\\Tests\\' => 'Tests'
		]
	]
];
